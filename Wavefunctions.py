#!/usr/bin/env python
# coding: utf-8

# Date: 01/06/2022 (wavefunction with nospin)
# 05/05/2022: Add a routine to initialize from a pyscf fci wavefunction
# 03/02/2022: Correct dimension A0,C0, AIJ, CIJ for n==N+1 or N==1
# 25/01/2022: Include the Transpositions module
# 12/01/2022: Add calculations of AIJ and CIJ and check relations
# 07/01/2022: Change Da and Dc into A0 and C0 to test with pyscf and the solution of full CI
# 17/02/2021: Define a product between Dyson matrices
# 16/02/2021: Use an attribute name to call the wavefunction
# 15/02/2021: Add one for init (all coef to 1) and passage to python 3
# 16/05/2018: add test of Da and Dc
# 27/04/2018: split into two files with Set_Wavefunctions.py
# 30/03/2018: correction of bug with Wavefunction.WW_cache
# 29/03/2018: add pair-density calculation
# 25/03/2018: when annihilate in |0>, return 0.0*|0>


import sys                       #Flush the stdout
import itertools                 #Iterators (combination, ...)
import numpy as np               #Linear algebra: common operations (matrix-matrix multiplications)
import scipy.linalg as linalg    #Linear algebra: eigensolvers (eigh)
from copy import deepcopy        #Deep copy of matrices
from scipy.special import binom  #Binom formula
from random import randint       #Random generator of integers


#Define a class many-body Wavefunction
class Wavefunction:
    """N-body Wavefunction with n states.
       For different N electrons and a constant n=number of states, 
       there is a symmetry between holes (n-N) and electrons (N).
       The equivalent state of |0> with no electron is |n> with no hole and only one single determinant."""
    CI_cache = {}  #Cache of indices for each Slater determinant (class variable!)
    bi_cache = {}  #Cache of binom numbers (class variable!)
    WW_cache = {}  #Cache of wavefunctions (given by wavefunction+annihilation or creation operator)
                   #Possibility of bug if the wavefunction is changed (as with normalize operation...)
                   #To avoid that only used internally!
    T_err = 1.e-13 #Threshold error for conditions
    def __init__(self,n,N,init=None,verbose=False,header=''):
        """Initialization from a (HF) Hartree-Fock, random configuration, uniform"""
        import numpy as np
        import itertools
        from scipy.special import binom
        self.n = n #Number of states
        self.n2 = self.n*self.n
        self.n2m = int(binom(self.n,2))
        self.N = N #Number of electrons
        #Works also for N=0: no index == scalar = state |0>
        #If N>n, then 0.0 (number of electrons is N)
        self.nCI = max( 1,int(binom(self.n,self.N)) )
        #Cache system
        if not (n,N) in Wavefunction.CI_cache:
            Wavefunction.CI_cache[ (n,N) ] = \
                    [ list(a) for a in itertools.combinations(range(1,self.n+1),self.N) ]
        self.CI = Wavefunction.CI_cache[ (n,N) ]
        if not (n,N) in Wavefunction.bi_cache:
            Wavefunction.bi_cache[(n,N)] = Wavefunction.build_bi(n,N)
        self.bi = Wavefunction.bi_cache[(n,N)]
        if verbose:
            print(header+"Number of orbitals, electrons and CI:", self.n,self.N,self.nCI)
        if isinstance(init,list):
            #List of indices
            #Give a list of indices to specify the state
            self.c = np.zeros(self.nCI)
            self.c[self.indice(init)] = 1.0
            self.name=str(init)
        elif isinstance(init,int):
            # Index of the indices
            self.c = np.zeros(self.nCI)
            self.c[init] = 1.0
            self.name=str(self.CI[init])
        elif init == "HF":
            #Use an Hartree-Fock state (the first one)
            self.c = np.zeros(self.nCI)
            self.c[0] = 1.0
            self.name=str(self.CI[0])
        elif init == "one":
            #All coefficients are equal to one (wavefunction not normalized)
            self.c = np.ones(self.nCI)
            self.name="one[N=%d]" % self.N
        elif init == "random":
            #Randomize
            self.c = np.add(np.random.rand(self.nCI),-0.5)
            self.name = "random"
            #Normalize
            self.normalize()
        elif init == "uniform":
            #All coefficients are equal
            fac = np.float64(1.0/np.sqrt(self.nCI))
            self.c = np.multiply( fac, np.ones(self.nCI) )
            self.name = "uniform[N=%d]" % self.N
        else:
            #Zeros
            self.c = np.zeros(self.nCI)
            self.name = "zero"
        #To check whether already calculated
        self.A0 = None
        self.C0 = None
        #
    def __add__(self,W):
        """Add two wavefunctions together"""
        w = Wavefunction(self.n,self.N)
        w.c = np.add(self.c,W.c)
        return w
    def __rmul__(self,a):
        """Multiply by a scalar (on the left)"""
        w = Wavefunction(self.n,self.N)
        w.c = np.multiply(a,self.c)
        return w
        #
    @staticmethod
    def reset_cache():
        """Reset all caches used with the Wavefunction class"""
        Wavefunction.CI_cache = {}
        Wavefunction.bi_cache = {}
        Wavefunction.WW_cache = {}
        #
    @staticmethod
    def indice_ref(n,N,l):
        """Find the index for the set of states given by l (no cache used)"""
        index = 0
        aprev = 1
        #print l,
        for j,a in enumerate(l):
            #print "<",j,a,aprev,">",
            for i in range(aprev,a):
                #print i,index,n-i,N-j-1,int(binom(n-i,N-j-1)),
                index += int(binom(n-i,N-j-1))
            aprev = a+1
        return index
        #
    @staticmethod
    def build_bi(n,N):
        """Build the binom numbers before call of indice (to use as cache)"""
        if N < 0:
            return [ ]
        bi = np.zeros( (n,N),dtype=int )
        for j in range(N):
            for i in range(n):
                bi[i,j] = int(binom(n-i,N-j-1))
        return bi
        #
    def indice(self,l):
        """Find the index for the set of states given by l
           optimized version: build an internal dictionary (memoization version)"""
        #We assume it is already calculated!!
        #if not (n,N) in Wavefunction.bi_cache:
        #    Wavefunction.bi_cache[(n,N)] = Wavefunction.build_bi(n,N)
        #bi = Wavefunction.bi_cache[(n,N)]
        index = 0
        aprev = 1
        #print l,
        for j,a in enumerate(l):
            #print "<",j,a,aprev,">",
            for i in range(aprev,a):
                #print j,i,bi[i,j],int(binom(n-i,N-j-1)),
                #index += int(binom(n-i,N-j-1))
                index += self.bi[i,j]
            aprev = a+1
        return index
        #
    def annihilation_ref(self,i):
        """Apply the annihilation operator a_i"""
        if self.N == 0:
            #Return 0.0*|0>
            return Wavefunction(self.n,self.N)
        w = Wavefunction(self.n,self.N-1)
        w.name = "a[%d]" % i + self.name if w.N > 0 else "[]"
        for j,l in enumerate(self.CI):
            if i in l:
                s = (-1)**l.index(i)
                la = list(l)
                la.remove(i)
                k = w.indice(la)
                w.c[k] = s*self.c[j]
        return w
        #
    def annihilation(self,i,cached=False):
        """Apply the annihilation operator a_i using iterator
           The option cached is private and should only used by the class methods."""
        if self.N == 0:
            #Return 0.0*|0>
            return Wavefunction(self.n,self.N)
        if cached and (self,'a',i) in Wavefunction.WW_cache:
            return Wavefunction.WW_cache[(self,'a',i)]
        w = Wavefunction(self.n,self.N-1)
        w.name = "a[%d]" % i + self.name if w.N > 0 else "[]"
        w_iter = enumerate(w.CI)
        for j,a in filter( lambda x: i in x[1], enumerate(self.CI) ):
            e = list(a)
            e.remove(i)
            while True:
                k,es = w_iter.__next__()
                if e == es:
                    break
            s = (-1)**a.index(i)
            w.c[k] = s*self.c[j]
        #Add in the cache
        if cached:
            Wavefunction.WW_cache[(self,'a',i)] = w
        return w
        #
    def dot_annihilation(self,i,w):
        """Apply the annihilation operator a_i and do the dot product i.e. <w|a_i|self>
           using iterators... (the best version)
           Convention [1,2,3,4] = a^+_1 a^+_2 a^+_3 a^+_4"""
        if w.N == 0:
            return 0.0
        if (w,'a',i) in Wavefunction.WW_cache:
            return np.sum( np.multiply(self.c,Wavefunction.WW_cache[(w,'a',i)].c) )
        elif (self,'c',i) in Wavefunction.WW_cache:
            return np.sum( np.multiply(Wavefunction.WW_cache[(self,'c',i)].c,w.c) )
        dot = 0.0
        self_iter = enumerate(self.CI)
        for j,a in filter( lambda x: i in x[1], enumerate(w.CI) ):
            e = list(a)
            e.remove(i)
            while True:
                k,es = self_iter.__next__()
                if e == es:
                    break
            s = (-1)**a.index(i)
            dot += self.c[k] * s*w.c[j]
        return dot
        #
    def dot_annihilation0(self,i,w):
        """Apply the annihilation operator a_i and do the dot product i.e. <w|a_i|self>
           using iterators."""
        if w.N == 0:
            return 0.0
        #w = Wavefunction(self.n,self.N-1)
        dot = 0.0
        c3_s,c3_index,c3 = zip( *[ (a[1].index(i),a[0],list( filterfalse(lambda x: x == i, a[1])) ) \
                for a in filter(lambda x: i in x[1], enumerate(w.CI) ) ] )
        c3 = list(c3)
        #print c3_s,c3_index,c3
        #print "self.CI",self.CI
        #c5 = [ a[0] for a in filter( lambda x: x[1] in c3, enumerate(self.CI)) ]
        c5 = []
        for k,a in enumerate(self.CI):
            if c3 and a == c3[0]:
                c5.append(k)
                c3.pop(0)
        #print c5
        for m,j,k in zip(c3_s,c3_index,c5):
            #print m,j,k
            s = (-1)**m
            dot += self.c[k] * s*w.c[j]
        return dot
        #
    def dot_annihilation_old(self,i,w):
        """Apply the annihilation operator a_i and do the dot product i.e. <self|a_i|w>"""
        if w.N == 0:
            return 0.0
        #w = Wavefunction(self.n,self.N-1)
        dot = 0.0
        for j,l in enumerate(w.CI):
            if i in l:
                s = (-1)**l.index(i)
                la = list(l)
                la.remove(i)
                k = self.indice(la)
                #print l.index(i),j,k
                dot += self.c[k] * s*w.c[j]
        return dot
        #
    def dot_annihilation_all(self,w):
        """Apply all annihilation operators a_i and do the dot product i.e. <self|a_i|w>
           using iterators...(too expensive!!)"""
        dot = self.n*[0.0]
        if w.N == 0:
            return dot
        kend = self.nCI-1
        #We annihilate for each index in each index 
        #we start with the first number for each index up to the Nth index
        for i1 in range(w.N):
            s = (-1)**i1
            #Cycle!
            self_iter = enumerate(self.CI)
            for j,a in enumerate(w.CI):
                e = list(a)
                i = e.pop(i1)
                while True:
                    k,es = self_iter.__next__()
                    if k == kend:
                        #reach the end regenerate (cycle is to expensive)
                        self_iter = enumerate(self.CI)
                    #print "w",j,a,i1,i,"self",k,es,e
                    if e == es:
                        break
                dot[i-1] += self.c[k] * s*w.c[j]
                #print "dot",dot,s,self.c[k],w.c[j]
        return dot
        #
    def creation_ref(self,i):
        """Apply the creation operator a^+_i"""
        w = Wavefunction(self.n,self.N+1)
        w.name = "c[%d]" % i + self.name
        for j,l in enumerate(self.CI):
            if not i in l:
                lc = list(l)
                lc.append(i)
                lc.sort()
                s = (-1)**lc.index(i)
                k = w.indice(lc)
                #print l,lc,k,s
                w.c[k] = s*self.c[j]
        return w
        #
    def creation(self,i,cached=False):
        """Apply the creation operator a^+_i using iterator
           cached is a private option and can be used only in the class methods"""
        if cached and (self,'c',i) in Wavefunction.WW_cache:
            return Wavefunction.WW_cache[(self,'c',i)]
        w = Wavefunction(self.n,self.N+1)
        w.name = "c[%d]" % i + self.name
        self_iter = enumerate(self.CI)
        for j,a in filter( lambda x: i in x[1], enumerate(w.CI) ):
            e = list(a)
            e.remove(i)
            while True:
                k,es = self_iter.__next__()
                if e == es:
                    break
            s = (-1)**a.index(i)
            w.c[j] = s*self.c[k]
        if cached:
            Wavefunction.WW_cache[(self,'c',i)] = w
        return w
        #
    def dot_creation(self,i,w):
        """Apply the creation operator a^+_i to w and do the dot product i.e. <self|a^+_i|w>
           (better to use the dot_annihilation operation which is more optimized and so faster)"""
        #w = Wavefunction(self.n,self.N+1)
        dot = 0.0
        for j,l in enumerate(w.CI):
            if not i in l:
                lc = list(l)
                lc.append(i)
                lc.sort()
                s = (-1)**lc.index(i)
                k = self.indice(lc)
                dot += self.c[k] * s*w.c[j]
        return dot
        #
    def dot(self,W):
        """Dot product over two many-body wavefunctions"""
        return np.sum( np.multiply(self.c,W.c) )
        #
    def prod_Dm(self,W):
        """Product between Dyson matrices of two many-body wavefunctions
           always identity times a scalar"""
        return np.einsum('im,km->ik',self.A0,self.A0) + np.einsum('km,im->ik',self.C0,self.C0)
    def dot_Dm(self,W):
        """Dot product over the Dyson matrices of two many-body wavefunctions
           We define by the scalar before the identity matrix
           Check also that the product is identity times a scalar"""
        C = self.prod_Dm(W)
        dd = C[0][0]
        if np.linalg.norm( C - np.multiply(dd,np.identity(self.n)) ) > 1.e-12:
            print("The product is not equal to an Identity matrix times a scalar!")
            print(self.name,W.name,C)
            raise ValueError
        return dd
    def norm(self):
        """Return the norm of the wavefunction"""
        return np.linalg.norm(self.c)
        #
    def normalize(self):
        """Normalize the wavefunction"""
        x = self.norm()
        if x != 0.0:
            #The normalisation is not possible if == 0.0
            #Raise an exception in the future?
            self.c = np.multiply(1.0/x,self.c)
        #
    def amplitudes_down(self,wm1=None,ortho=True,header='amplitudes_down '):
        """Calculate the transition amplitudes of the given wavefunction over a set of N-1 state wavefunctions"""
        if wm1 == None:
            #Normalize the wavefunctions (they are not orthonormalized -- do not use cache because normalize)
            wm1 = [ self.annihilation(i+1) for i in range(self.n) ]
            for w in wm1:
                w.normalize()
            if ortho:
                wm1 = orthonormalize( wm1,keep=min(self.n,int(binom(self.n,self.N-1))),header=header )
        self.wm1 = wm1
        self.A0 = np.array([ [ wm.dot_annihilation(i+1,self) for wm in wm1 ] for i in range(self.n) ])
        #
    def amplitudes(self,wm1=None,wp1=None,ortho=True,header='amplitudes '):
        """Calculate the transition amplitudes of the given wavefunction 
        over two sets of wavefunctions for N-1 and N+1"""
        #First calculate the amplitudes A0
        self.amplitudes_down(wm1=wm1,ortho=ortho,header=header)
        #Then the amplitudes C0
        if wp1 == None:
            #Normalize the wavefunctions (they are not orthonormalized!)
            wp1 = [ self.creation(i+1) for i in range(self.n) ]
            for w in wp1:
                w.normalize()
            if ortho:
                wp1 = orthonormalize( wp1,keep=min(self.n,int(binom(self.n,self.N+1))),header=header )
        self.wp1 = wp1
        self.C0 = np.array([ [ self.dot_annihilation(i+1,wp) for wp in wp1] for i in range(self.n) ])
        #
    def one_density_matrix(self,wm1=None):
        """Calculate the one-body density matrix (straight way from wm1 for references)
        if wm1 is given, should be self.annihilation(i)"""
        if wm1 == None:
            wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        self.G1 = np.zeros( 2*(self.n,) )
        for i in range(self.n):
            for j in range(self.n):
                self.G1[i,j] = wm1[i].dot(wm1[j])
        (self.n1,self.U1) = np.linalg.eigh(self.G1)
        #
    def one_density_matrix_from_amplitudes(self):
        """Calculate the one-body density matrix from the transition amplitudes"""
        if not isinstance(self.A0,np.ndarray):
            print("one_density_matrix_from_amplitudes: recalculate amplitudes")
            self.amplitudes_down()
        self.G1 = np.einsum('im,km->ik',self.A0,self.A0)
        (self.n1,self.U1) = np.linalg.eigh(self.G1)
        #
    def pair_density(self):
        """Calculate the pair density"""
        P = np.zeros( (self.n,self.n) )
        for I,c in zip(self.CI,self.c):
            for (i,j) in itertools.combinations(I,2):
                P[i-1,j-1] += c*c
        return P
        #
    def build_AIJ(self,header=''):
        """Calculate the AIJ transition amplitudes"""
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        wm2 = [ w.annihilation(i+1,cached=True) for i in range(self.n) for w in wm1 ]
        #Orthonormalize the wm2
        wmo2 = orthonormalize(wm2,header=header+'build_AIJ - ',keep=min( int(binom(self.n,2)), int(binom(self.n,self.N-2)) ) )
        print(header+'build_AIJ - len(wm1)',len(wm1),'len(wm2)',len(wm2), \
              'len(wmo2)',len(wmo2),'(n 2)',int(binom(self.n,2)),'(n N-2)',int(binom(self.n,self.N-2)))
        self.AIJ = np.array([ [ [ wo.dot(w.annihilation(j+1,cached=True)) for wo in wmo2] for j in range(self.n) ] for w in wm1])
        #
    def build_CIJ(self,header=''):
        """Calculate the CIJ transition amplitudes"""
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        wmp = [ w.creation(i+1,cached=True) for i in range(self.n) for w in wm1 ]
        #Orthonormalize the wmp
        wmpo = orthonormalize(wmp,header=header+'build_CIJ - ',keep=min(self.n**2,int(binom(self.n,self.N)) ) )
        print(header+'build_CIJ - len(wm1)',len(wm1),'len(wmp)',len(wmp),\
              'len(wmpo)',len(wmpo),'n**2',self.n**2,'(n N)',int(binom(self.n,self.N)))
        self.CIJ = np.array([ [ [ wo.dot(w.creation(j+1,cached=True)) for wo in wmpo] for j in range(self.n) ] for w in wm1])
        #
    def DM_EEEE(self):
        """Calculate the two-body density matrix a^+a^+aa
           (straight way for references) - anti-symmetric"""
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        wm2 = [ [ w.annihilation(i+1,cached=True) for i in range(self.n) ] for w in wm1 ]
        self.EEEE = np.zeros( 4*(self.n,) )
        for k,wk in enumerate(wm2):
            for i,wi in enumerate(wk):
                for l,wl in enumerate(wm2):
                    for j,wj in enumerate(wl):
                        self.EEEE[k,i,l,j] = wi.dot(wj)
        #
    def DM_EHEH(self):
        """Calculate the eheh two-body density matrix i.e. a^+aa^+a"""
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        w0 = [ [ w.creation(i+1,cached=True) for i in range(self.n) ] for w in wm1 ]
        self.EHEH = np.zeros( 4*(self.n,) )
        for k,wk in enumerate(w0):
            for i,wi in enumerate(wk):
                for l,wl in enumerate(w0):
                    for j,wj in enumerate(wl):
                        self.EHEH[k,i,l,j] = wi.dot(wj)
        #
    def DM_HEHE(self):
        """Calculate the hehe two-body density matrix i.e. aa^+aa^+"""
        wp1 = [ self.creation(i+1,cached=True) for i in range(self.n) ]
        w0 = [ [ w.annihilation(i+1,cached=True) for i in range(self.n) ] for w in wp1 ]
        self.HEHE = np.zeros( 4*(self.n,))
        for k,wk in enumerate(w0):
            for i,wi in enumerate(wk):
                for l,wl in enumerate(w0):
                    for j,wj in enumerate(wl):
                        self.HEHE[k,i,l,j] = wi.dot(wj)
        #
    def DM_HHHH(self):
        """Calculate the hhhh two-body density matrix i.e. aaa^+a^+
           anti-symmetric"""
        wp1 = [ self.creation(i+1,cached=True) for i in range(self.n) ]
        wp2 = [ [ w.creation(i+1,cached=True) for i in range(self.n) ] for w in wp1 ]
        self.HHHH = np.zeros((self.n,self.n,self.n,self.n))
        for k,wk in enumerate(wp2):
            for i,wi in enumerate(wk):
                for l,wl in enumerate(wp2):
                    for j,wj in enumerate(wl):
                        self.HHHH[k,i,l,j] = wi.dot(wj)
        #
    def DM_T1(self,verbose=False):
        """Calculate the T1 conditions = 3D + 3Q (linear of combination only on EEEE)
           3D = a^+i a^+j a^+k as aq ap
           3Q = ap aq as a^+k a^+j a^+i"""
        self.T1 = np.zeros( 6*(self.n,) )
        # First term a^+i a^+j a^+k as aq ap
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        wm2 = [ [ w1.annihilation(i+1,cached=True) for i in range(self.n) ] for w1 in wm1 ]
        wm3 = [ [ [ w2.annihilation(i+1,cached=True) for i in range(self.n) ] for w2 in w1 ] for w1 in wm2 ]
        for p,wp in enumerate(wm3):
            for q,wp in enumerate(wp):
                for s,ws in enumerate(wp):
                    for i,wi in enumerate(wm3):
                        for j,wj in enumerate(wi):
                            for k,wk in enumerate(wj):
                                self.T1[i,j,k,p,q,s] = wk.dot(ws)
        #Second term ap aq as a^+k a^+j a^+i
        wp1 = [ self.creation(i+1,cached=True) for i in range(self.n) ]
        wp2 = [ [ w1.creation(i+1,cached=True) for i in range(self.n) ] for w1 in wp1 ]
        wp3 = [ [ [ w2.creation(i+1,cached=True) for i in range(self.n) ] for w2 in w1 ] for w1 in wp2 ]
        for i,wi in enumerate(wp3):
            for j,wj in enumerate(wi):
                for k,wk in enumerate(wj):
                    for p,wp in enumerate(wp3):
                        for q,wp in enumerate(wp):
                            for s,ws in enumerate(wp):
                                self.T1[i,j,k,p,q,s] += ws.dot(wk)
        self.t1 = linalg.eigvalsh(self.T1.reshape( 2*(self.n2*self.n,) ))
        if verbose:
            print("T1 conditions:",self.t1[0], np.all(self.t1>-1.e-13),\
                    "[%d/%d]" % (len(np.where(abs(self.t1) < 1.e-12)[0]),len(self.t1)) )
        return np.all(self.t1 > -Wavefunction.T_err)
        #
    def DM_T2(self,verbose=False):
        """Calculate the T2 conditions = 3E + 3F (linear of combination only on EEEE)
           3E = a^+i a^+j ak a^+s aq ap
           3F = ap aq a^+s ak a^+j a^+i"""
        self.T2 = np.zeros( 6*(self.n,) )
        # First term a^+i a^+j ak a^+s aq ap
        wm1 = [ self.annihilation(i+1,cached=True) for i in range(self.n) ]
        wm2 = [ [ w1.annihilation(i+1,cached=True) for i in range(self.n) ] for w1 in wm1 ]
        wp3 = [ [ [ w2.creation(i+1,cached=True) for i in range(self.n) ] for w2 in w1 ] for w1 in wm2 ]
        for p,wp in enumerate(wp3):
            for q,wp in enumerate(wp):
                for s,ws in enumerate(wp):
                    for i,wi in enumerate(wp3):
                        for j,wj in enumerate(wi):
                            for k,wk in enumerate(wj):
                                self.T2[i,j,k,p,q,s] = wk.dot(ws)
        #Second term ap aq a^+s ak a^+j a^+i
        wp1 = [ self.creation(i+1,cached=True) for i in range(self.n) ]
        wp2 = [ [ w1.creation(i+1,cached=True) for i in range(self.n) ] for w1 in wp1 ]
        wm3 = [ [ [ w2.annihilation(i+1,cached=True) for i in range(self.n) ] for w2 in w1 ] for w1 in wp2 ]
        for i,wi in enumerate(wm3):
            for j,wj in enumerate(wi):
                for k,wk in enumerate(wj):
                    for p,wp in enumerate(wm3):
                        for q,wp in enumerate(wp):
                            for s,ws in enumerate(wp):
                                self.T2[i,j,k,p,q,s] += ws.dot(wk)
        self.t2 = linalg.eigvalsh(self.T2.reshape( 2*(self.n2*self.n,) ))
        if verbose:
            print("T2 conditions:",self.t2[0], "n0=",np.all(self.t2>-1.e-13), \
                    "[%d/%d]" % (len(np.where(abs(self.t2) < 1.e-12)[0]),len(self.t2)) )
        return np.all(self.t2 > -Wavefunction.T_err)
        #
    def ACDM(self,header=''):
        """Calculate the ACDM"""
        self.amplitudes(header=header)
        self.one_density_matrix()
        self.build_AIJ(header=header)
        self.build_CIJ(header=header)
        #
    def DM_all(self,header=''):
        """Calculate all two-body density matrices"""
        self.ACDM(header=header)
        self.DM_EEEE()
        self.DM_EHEH()
        self.DM_HEHE()
        self.DM_HHHH()
        #
    def DM_check_relations(self,verbose=False):
        """Check relations between DM"""
        self.I = np.identity(self.n)
        #Check we find the same one-body density
        diffG1 = np.linalg.norm( self.G1 - np.einsum('im,km->ik',self.A0,self.A0) )
        #Check D0_Constraints: A0.T @ A0 + C0.T @ C0 = I_n
        diffD0 = np.linalg.norm( np.einsum('im,km->ik',self.A0,self.A0) + np.einsum('km,im->ik',self.C0,self.C0) - self.I )
        #Check A0_Constraints
        diffA0 =  np.linalg.norm(  np.einsum('im,im->',self.A0,self.A0) - np.float64(self.N) )
        #Check DIJ_Constraints
        diffDIJ = np.linalg.norm( \
                np.einsum('ijm,klm->ikjl',self.AIJ,self.AIJ) + np.einsum('ijm,klm->iklj',self.CIJ,self.CIJ) - np.einsum('im,jm,kl->ijkl',self.A0,self.A0, self.I))
        #Check AIJ_Constraints
        diffAIJ = np.linalg.norm( np.einsum('ijm,kjm->ik',self.AIJ,self.AIJ) - np.einsum('im,km->ik',self.A0,self.A0)*np.float64(self.N-1) )
        #Check with find EHEE with AIJ
        diffEEEE = np.linalg.norm( np.einsum('kilj->klij',self.EEEE) - np.einsum('ijm,klm->ikjl',self.AIJ,self.AIJ) )
        #Check with find T2(EHEH) with CIJ
        diffEHHE = np.linalg.norm( np.einsum('kilj->klij',T2(self.EHEH)) - np.einsum('ijm,klm->iklj',self.CIJ,self.CIJ) )
        # Conditions over Density matrices
        self.G1I = np.kron(self.G1,self.I).reshape( 4*(self.n,) )
        self.IG1I = np.kron(self.I-self.G1,self.I).reshape(4*(self.n,))
        self.II = np.kron(self.I,self.I).reshape( 4*(self.n,) )
        #Check EEEE + T2(EHEH) = G1*I
        diff1 = np.linalg.norm( self.EEEE + T2(self.EHEH) - self.G1I )
        #Check HHHH + T2(HEHE) = (I-G1)*I
        diff2 = np.linalg.norm( self.HHHH + T2(self.HEHE) - self.IG1I )
        #Check HEHE = outer(I,I) - outer(I,G1) - outer(G1,I) + T0(EHEH) - old fashion
        #DIFF = np.zeros_like(self.EEEE)
        #for l in range(self.n):
        #    for i in range(self.n):
        #        for k in range(self.n):
        #            for j in range(self.n):
        #                DIFF[l,i,k,j] = self.I[l,i]*self.I[k,j] - self.I[l,i]*self.G1[k,j] - self.G1[l,i]*self.I[k,j] \
        #                              + self.EHEH[i,l,j,k] - self.HEHE[l,i,k,j]
        #diff3 = np.linalg.norm(DIFF)
        diff3 = np.linalg.norm(np.outer(self.I,self.I) - np.outer(self.I,self.G1) - np.outer(self.G1,self.I) \
                + (T0(self.EHEH)-self.HEHE).reshape(self.n2,self.n2) )
        if verbose:
            print("G1-A0.T@A0",diffG1)
            print("EEEE-AIJ.AIJ",diffEEEE)
            print("EHHE-CIJ.CIJ",diffEHHE)
            print("D0_Constraints",diffD0)
            print("A0_Constraints",diffA0)
            print("DIJ_Constraints",diffDIJ)
            print("AIJ_Constraints",diffAIJ)
            print("EEEE+T2(EHEH)=kron(G1,I)",diff1)
            print("HHHH+T2(HEHE)=kron(I-G1,*I)",diff2)
            print("HEHE=outer(I,I)-outer(I,G1)-outer(G1,I)+T0(EHEH)=",diff3)
        #
        Tdiff = diffD0+diff1+diff2+diff3
        #Other test
        #EEEE + T2(EHEH) + T1(HEHE) + HHHH = I*I
        err = np.linalg.norm(self.EEEE + T2(self.EHEH) + T1(self.HEHE) + self.HHHH - self.II)
        print("EEEE + T2(EHEH) + T1(HEHE) + HHHH = kron(I,I)", err)
        Tdiff += err
        err = np.linalg.norm(self.EEEE + T2(self.EHEH+self.HEHE) + self.HHHH - self.II)
        print("EEEE + T2(EHEH+HEHE) + HHHH = kron(I,I)", err)
        Tdiff += err
        err = np.linalg.norm(self.EEEE+self.HHHH+self.EHEH+self.HEHE \
                + T2(self.EEEE+self.HHHH+self.EHEH+self.HEHE) - 2*self.II)
        print("EEEE+EHEH+HEHE+HHHH + T2(EEEE+EHEH+HEHE+HHHH) = 2 kron(I,I)", err)
        Tdiff += err
        #EEEE + T2(EHEH) = G1*I
        print("EEEE + T2(EHEH) = G1*I", np.linalg.norm(self.EEEE + T2(self.EHEH) - self.G1I))
        print("EEEE + T1(EHEH) = G1*I", np.linalg.norm(self.EEEE + T1(self.EHEH) - self.G1I))
        #T1(EEEE) = T2(EEEE)
        print("T1(EEEE) = T2(EEEE)", np.linalg.norm(T2(self.EEEE)-T1(self.EEEE)))
        #T1(EHEH) = T2(EHEH)
        print("T1(EHEHE) = T2(EHEH)", np.linalg.norm(T2(self.EHEH)-T1(self.EHEH)))
        #HHHH + T2(HEHE) = G1*I
        print("HHHH + T2(HEHE) = (I-G1)*I", np.linalg.norm(self.HHHH + T2(self.HEHE) - self.IG1I))
        print("HHHH + T1(HEHE) = (I-G1)*I", np.linalg.norm(self.HHHH + T1(self.HEHE) - self.IG1I))
        #T1(HHHH) = T2(HHHH)
        print("T1(HHHH) = T2(HHHH)", np.linalg.norm(T2(self.HHHH)-T1(self.HHHH)))
        #T1(HEHE) = T2(HEHE)
        print("T1(EHEHE) = T2(EHEH)", np.linalg.norm(T2(self.HEHE)-T1(self.HEHE)))
        #EHEH + HEHE = T1(T2(EHEH+HEHE))
        print("EHEH + HEHE = T1(T2(EHEH+HEHE))", np.linalg.norm(self.EHEH + self.HEHE - T1(T2(self.EHEH+self.HEHE))))
        #T2(self.EHEH) + T1(self.HEHE) = T1(self.EHEH) + T2(self.HEHE)
        print("T2(self.EHEH) + T1(self.HEHE) = T1(self.EHEH) + T2(self.HEHE)", \
            np.linalg.norm(T2(self.EHEH) + T1(self.HEHE) - T1(self.EHEH) - T2(self.HEHE)) )
        return Tdiff
        #
    def positivity(self,verbose=False):
        """Check the positivity of the four density matrices"""
        self.eeee = linalg.eigvalsh(self.EEEE.reshape( 2*(self.n2,) ))
        self.eheh = linalg.eigvalsh(self.EHEH.reshape( 2*(self.n2,) ))
        self.hehe = linalg.eigvalsh(self.HEHE.reshape( 2*(self.n2,) ))
        self.hhhh = linalg.eigvalsh(self.HHHH.reshape( 2*(self.n2,) ))
        if verbose:
            print("D conditions: positivity of EEEE =",self.eeee)
            print("Q conditions: positivity of HHHH =",self.hhhh)
            print("G conditions: positivity of EHEH =",self.eheh)
            print("[positivity of HEHE] =",self.hehe)
        return np.all(self.eeee>-Wavefunction.T_err) and \
               np.all(self.eheh>-Wavefunction.T_err) and \
               np.all(self.hehe>-Wavefunction.T_err) and \
               np.all(self.hhhh>-Wavefunction.T_err)


def test_indices():
    """Test the indice routine"""
    print("Test the calculation of the indices:",)
    OK = True
    for n in range(10):
        for N in range(n+1):
            print("[%d-%d:" % (n,N),)
            W0 = Wavefunction(n,N)
            for i,l in enumerate(W0.CI):
                ind = W0.indice(l)
                if i != ind:
                    print("Error",i,"-- %d --" % W0.indice(l),l)
                    OK = False
                else:
                    print("%d" %i,)
            print("]",)
    return OK


def test_creation_annihilation():
    """Test creation and annihilation routines with a^+_i a_i + a_i a^+_i = 1"""
    from random import randint
    print("Test the creation and annihilation routines:",)
    OK = True
    for i in range(30):
        n = randint(1,15)
        N = randint(1,n)
        W0 = Wavefunction(n,N,init="random")
        for j in range(1,W0.n+1):
            wa = W0.annihilation(j).creation(j)
            wc = W0.creation(j).annihilation(j)
            if abs(sum(W0.c - (wa+wc).c)) > 1.e-14:
                OK = False
                print(W0.n,W0.N,W0.c,W0.c - (wa+wc).c)
            else:
                print("[%d - %d: %d]" % (W0.n,W0.N,j),)
    return OK


def orthonormalize(wms,header='', verbose=False, keep=None):
    """Orthonormalize a set of wavefunctions.
       if keep == None, remove norm == 0.0 and linear dependency
                         else add orthonormalized random wavefunctions up to the keep number"""
    ws2 = wms
    #Remove wavefunctions with norm == 0 and normalize the wavefunctions
    nu = 1
    ipass = 0
    while nu != 0:
        ws1 = []
        ipass += 1
        if verbose:
            print(header+"orthonormalize: ipass=%d" % ipass)
        for ws in ws2:
            x = ws.norm()
            if x != 0.0:
                ws1.append(ws)
        nu = len(ws2) - len(ws1)
        if nu > 0 and verbose:
            print(header+"orthonormalize: %d null wavefunctions over %d" % (nu,len(wms)))
        if len(ws1) <= 1:
            ws2 = ws1
            break
        #Overlap matrix
        l = len(ws1)
        G1 = np.zeros((l,l))
        for i,wsi in enumerate(ws1):
            for j,wsj in enumerate(ws1):
                G1[i,j] = wsi.dot(wsj)
        (n1,U1) = np.linalg.eigh(G1)
        if verbose:
            print(header+"orthonormalize - diag(G1)",np.diag(G1))
            print(header+"orthonormalize - n1",n1)
        #Orthonormalize
        ws2 = []
        for i in range(l):
            #Remove linear dependent wavefunctions if keep == False
            if n1[i] < 1.e-15:
                continue
            #We compute this ws
            ws = Wavefunction(ws1[0].n,ws1[0].N)
            for j in range(l):
                ws += U1[j][i] * ws1[j]
            #Normalize and append
            ws.normalize()
            ws2.append(ws)
        nu = len(ws1) - len(ws2)
        if nu > 0:
            if verbose:
                print(header+"orthonormalize: %d linear dependent wavefunctions over %d" % (nu,len(ws1)))
        if ipass > 3:
            print(header+"orthonormalize: more than 3 passes (cycle=%d)-> Trouble!" % ipass)
            break
    if keep != None:
        if isinstance(keep,int):
            nu = keep - len(ws2)
        else:
            nu = len(wms) - len(ws2)
        print(header+'orthonormalize: keep,nu,len(ws2),len(wms) --',keep,nu,len(ws2),len(wms))
        if nu < 0:
            #We remove -nu wavefunctions
            print(header+"orthonormalize: Remove %d randomized orthonormalized wavefunctions" % -nu)
            for i in range(-nu):
                ws2.pop()
            nu = 0
        else:
            #We generate orthonormalized randomized wavefunctions in order to have the required number
            print(header+"orthonormalize: Generate %d randomized orthonormalized wavefunctions" % nu)
            for i in range(nu):
                ws = Wavefunction(wms[0].n,wms[0].N,init="random")
                for j in range(len(ws2)):
                    ws += - ws.dot(ws2[j])*ws2[j]
                ws2.append(ws)
        assert len(ws2) == keep, "The orthonormalization does not keep the right number (%0d/%0d)\n" % (len(ws2),keep)
    if verbose:
        l = len(ws2)
        A0 = np.zeros((l,l))
        for i in range(l):
            for j in range(l):
                A0[i,j] = ws2[i].dot(ws2[j])
        print(header+"orthonormalize - nW=%d" % l,"diag(A0)" ,np.diag(A0))
        print(header+"orthonormalize - check: ",np.linalg.norm(A0-np.eye(l)))
    return ws2


def Test_sum_creation_annihilation():
    """Check that the N operator gives N*the wavefunction"""
    #Random wavefunction
    nw = 10 ; Nw = 5
    WW = Wavefunction(nw,Nw,init="random")
    WN = Wavefunction(nw,Nw)
    for i in range(nw):
        #print i+1,WW.annihilation(i+1).creation(i+1).c
        WN += WW.annihilation(i+1).creation(i+1)
    WN = 1.0/np.float64(Nw)* WN
    Tdiff = np.linalg.norm(WW.c-WN.c)
    print("Check the N operator err=",Tdiff)
    return Tdiff


#from the file Transpositions.py

#Define the special transpositions related to the two-body density matrices
def T0(DDP):
    """Special transposition (k,i,h,j) -> (i,k,j,h) EHEHE and HEHE are not anti-symmetric!"""
    s = DDP.shape
    if len(s) == 2:
        n = int(np.sqrt(DDP.shape[0]))
        return DDP.reshape( 4*(n,) ).transpose(1,0,3,2).reshape(n*n,n*n)
    else:
        return DDP.transpose(1,0,3,2)

def T1(DDP):
    """Special transposition (k,i,h,j) -> (h,i,k,j)"""
    s = DDP.shape
    if len(s) == 2:
        n = int(np.sqrt(DDP.shape[0]))
        return DDP.reshape( 4*(n,) ).transpose(2,1,0,3).reshape(n*n,n*n)
    else:
        return DDP.transpose(2,1,0,3)

def T2(DDP):
    """Special transposition (k,i,h,j) -> (k,j,h,i)"""
    s = DDP.shape
    if len(s) == 2:
        n = int(np.sqrt(DDP.shape[0]))
        return DDP.reshape( 4*(n,) ).transpose(0,3,2,1).reshape(n*n,n*n)
    else:
        return DDP.transpose(0,3,2,1)

#T1(T2) is the symmetry: (k,i,h,j) -> (h,j,k,i) with scalar product


def Display_M(n):
    """Build a matrix with indices to represent the different transpositions"""
    a = np.zeros( (n,n), dtype=("S",2) )
    for i in range(1,n+1):
        for j in range(1,n+1):
            #print i,j,"%d%d" % (i,j)
            a[i-1,j-1] = "%d%d" % (i,j)
    b = np.zeros( (n*n,n*n), dtype=("S",5) )
    for i,u in enumerate(a.ravel()):
        for j,d in enumerate(a.ravel()):
            b[i,j] = d+"."+u
    return b

def one_order(a):
    """Order one element i.e. 21.12 -> 12.21"""
    b = a.split('.')
    b.sort()
    return "%s.%s" % tuple(b)

def Order(M):
    """Order the string for each element i.e. 21.12 -> 12.21"""
    Mo = deepcopy(M)
    for x in np.nditer(Mo, op_flags=['readwrite']):
        x[...] = one_order(str(x))
    return Mo

def Display_T0(n):
    """Display matrix of indices corresponding to T0"""
    return T0(Display_M(n))


def Display_T1(n):
    """Display matrix of indices corresponding to T1"""
    return T1(Display_M(n))


def Display_T2(n):
    """Display matrix of indices corresponding to T2"""
    return T2(Display_M(n))


def Wavefunction_from_pyscf(norb,nelec,fcivec,header=''):
    """Create a wavefunction from a FCI wavefunction calculated by pyscf"""
    W0 = Wavefunction(norb,nelec,header='')
    fci_coef = fcivec.flatten()
    import itertools
    #print([ list(a) for a in W0.CI ])
    #list of int64.  One int64 element represents one string in binary format.
    #The binary format takes the convention that the one bit stands for one orbital, bit-1 means occupied and bit-0 means unoccupied.
    #The lowest (right-most) bit corresponds to the lowest orbital in the orb_list.
    from pyscf.fci import cistring
    #ll = [bin(x) for x in cistring.make_strings((0,1,2,3),2)]
    ll = [ bin(x) for x in cistring.make_strings(np.arange(norb),nelec)]
    #print(ll)
    def citranspose(x):
        s = l[2:][::-1]
        aa = []
        for i,v in enumerate(s):
            #print(s,i,v)
            if v == '1':
                aa.append(i+1)
        return aa
    for i,l in enumerate(ll):
        lci = citranspose(l)
        nlci = Wavefunction.indice_ref(W0.n,W0.N,lci)
        #print(lci,nlci,lci,W0.CI[nlci])
        W0.c[nlci] = fci_coef[i]
    return W0


#Some operations over matrices
#EEEE + T2(EHEH+HEHE) + HHHH = np.kron(I,I)
#EEEE + T2(EHEH) = np.kron(G1,I)
#HHHH + T2(HEHE) = np.kron(I-G1,I)
#HEHE = np.outer(I,I) - np.outer(G1,I) - np.outer(I,G1 + T0(EHEH)
#     = np.outer( (I-G1), (I-G1) ) - np.outer(G1,G1) + T0(EHEH)
#EHEH = np.kron(G1,I)- np.kron(I,G1) + T0(EHEH)
#HHHH = EEEE + np.kron(I-G1,I) - np.kron(I,G1) - T2(np.outer(I-G1,I) - np.outer(I,G1))

if __name__ == "__main__":
    print("--- Check sum creation.annihilation = Wavefunction n=10, N=5" + 60*"-")
    Tdiff = Test_sum_creation_annihilation()
    #
    print("--- Check D, Q and G conditions n=4, N=2" + 60*"-")
    #Random wavefunction
    W0 = Wavefunction(4,2,init="random")
    #Remove some coefficients to have states 1 and 3 as natural orbitals
    W0.c[1] = W0.c[2] = W0.c[3] = W0.c[4] = 0.0
    print(W0.CI)
    print(W0.c)
    W0.normalize()
    print("n=",W0.n," N=",W0.N)
    W0.DM_all()
    Tdiff += W0.DM_check_relations(verbose=True)
    W0.positivity(verbose=True)
    W0.DM_T1(verbose=True)
    W0.DM_T2(verbose=True)
    #
    print("--- Check D, Q and G conditions for 3 wavefunctions" + 60*"-")
    for iter in range(3):
        n = randint(5,12)
        N = randint(2,n-3)
        W0 = Wavefunction(n=n,N=N,init="random")
        W0.DM_all()
        test1 = W0.positivity(verbose=False)
        eeee = len(np.where(abs(W0.eeee) < 1.e-12)[0]) - (W0.n2-W0.n2m)
        eheh = len(np.where(abs(W0.eheh) < 1.e-12)[0])
        hehe = len(np.where(abs(W0.hehe) < 1.e-12)[0])
        hhhh = len(np.where(abs(W0.hhhh) < 1.e-12)[0]) - (W0.n2-W0.n2m)
        iter += 1
        in2m = W0.n2m -1
        test2 = W0.DM_T1(verbose=True)
        test3 = W0.DM_T2(verbose=True)
        test = test1 and test2 and test3
        text = "%3d n=%2d N=%2d n2=%3d eeee=%9.2e %3d eheh=%9.2e %3d hehe=%9.2e %3d hhhh=%9.2e %3d %r" % \
                (iter,n,N,W0.n2,W0.eeee[in2m],eeee, W0.eheh[0],eheh, W0.hehe[0],hehe, W0.hhhh[in2m],hhhh, test)
        print(text)
    print("Total difference of all tests",Tdiff)
