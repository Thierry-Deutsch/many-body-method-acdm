#!/usr/bin/env python
# coding: utf-8

#Copyright CEA
#Author: Thierry Deutsch
#Date: 18/07/2022

# 18/07/2022 -- Correct method for augmented lagrangian and penalty method 
# 11/07/2022 -- Add routine to convert a spin Hamiltonian to a non-spin Hamiltonian doubling the dimension
# 13/06/2022 -- Add manipulation on the Hamiltonian
# 31/05/2022 -- Gradient Lagrangian are not zero (ex. 6 3). need a check for that.
#To be done:
# - Determine multipliers from Lagrangian derivatives of G_e, G_h, G_ee, and G_eh
# - Why Lagrangian derivatives are not fully zero? (feasible conditions)
# - Correct augmented Lagrangian algorithm
# 20/05/2022 -- Add a FCI class to handle FCI related objects
# 17/05/2022 -- Add all stuff to build ACDM from FCI pyscf

#Build an ACDM from Wavefunction

import ACDM
import pyscf
import scipy
import termcolor as tc
from Wavefunctions import *


class FCI:
    """Class to handle FCI from pyscf"""
    # pspace function computes the FCI Hamiltonian for "primary" determinants.
    # Primary determinants are the determinants which have lowest expectation
    # value <H>.  np controls the number of primary determinants.
    # To get the entire Hamiltonian, np should be larger than the wave-function
    # size.  In this example, a (8e,7o) FCI problem has 1225 determinants.
    def __init__(self,h1,h2,nelec,header=''):
        from scipy.special import binom  #Binom formula
        from pyscf import fci
        norb = h1.shape[0]
        ndet = int(binom(norb,nelec))
        H_fci = fci.direct_spin1.pspace(h1, h2, norb, [nelec,0], np=ndet)[1]
        e_all, v_all = np.linalg.eigh(H_fci)

        FCI.efci, FCI.fcivec = fci.direct_spin1.kernel(h1, h2, norb, [nelec,0], nroots=1,
                                        max_space=30, max_cycle=50)
        #print('First root:')
        print(header+'FCI energy linalg,direct_spin1:', e_all[0],FCI.efci, \
              'wfn overlap', np.linalg.norm(v_all.dot(FCI.fcivec.ravel())))

        FCI.rdm1, FCI.rdm2 = fci.direct_spin1.make_rdm12(FCI.fcivec, norb, [nelec,0])
        FCI.energy = np.einsum('ij,ij->',h1,FCI.rdm1)+0.5*np.einsum('ijkl,ijkl->',h2,FCI.rdm2)
        print(header+'FCI energy', FCI.efci,'using rdm1 and rdm2',FCI.energy)


def Depack_W0(acdm,W0):
    """From Wavefunction, build triangular ACDM"""
    return ACDM.QR_decomposition(acdm,W0.A0,W0.C0,W0.AIJ,W0.CIJ)


def Determine_multipliers(acdm,A0,C0,AIJ,CIJ,X0=0,gtol=1.e-6,header=''):
    """Determine Lagrange multipliers from least-square algorithm"""
    def gradients(X):
        Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = acdm.Depack_XL(X)
        X0 = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        return acdm.Gradient_Lagrangian(X0)
    LambdaIJ = np.zeros( (acdm.n,)*4 )
    if isinstance(X0,int):
        Lambda0 = np.zeros( (acdm.n,)*2 )
        muA0 = 0.0
        muC0 = 0.0
        muAI = np.zeros( (acdm.n,)*2 )
        muCI = np.zeros( (acdm.n,)*2 )
    else:
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    XL = acdm.Pack_XL(Lambda0,muA0,muC0,LambdaIJ,muAI,muCI)
    print(header+'-'*20,'Determine the Lagrange multipliers from least square algorithms (gtol=%8.2e)' % gtol)
    sol_LS = scipy.optimize.least_squares(fun=gradients, x0=XL,verbose=2,gtol=gtol)#,method='lm')
    XL = sol_LS.x
    if acdm.onechemical:
        muC0 = 0.0
        muCI = np.zeros( (acdm.n,)*2 )
    Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = acdm.Depack_XL(XL)
    print(header+'-'*20,'Gradient norm',np.linalg.norm(gradients(XL)))
    return Lambda0,muA0,muC0,LambdaIJ,muAI,muCI


def ACDM_from_pyscf(acdm,norb,nelec,FCI,multipliers=True,gtol=1.e-6,header=''):
    """Build ACDM from pyscf solution"""
    #
    W0 = Wavefunction_from_pyscf(norb,nelec,FCI.fcivec,header=header)    
    W0.DM_all(header=header)
    A0,C0,AIJ,CIJ = Depack_W0(acdm,W0)
    if multipliers:
        Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = Determine_multipliers(acdm,A0,C0,AIJ,CIJ,gtol=gtol,header=header)
    else:
        LambdaIJ = np.zeros( (acdm.n,)*4 )
        Lambda0 = np.zeros( (acdm.n,)*2 )
        muA0 = 0.0
        muC0 = 0.0
        muAI = np.zeros( (acdm.n,)*2 )
        muCI = np.zeros( (acdm.n,)*2 )
    X0 = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
    AA0 = np.einsum('im,Im->iI',A0,A0)
    AAIJ = np.einsum('ijm,IJm->iIjJ',AIJ,AIJ)
    W0_ACDM_energy = np.einsum('ij,ij->',acdm.H1,AA0)+0.5*np.einsum('ijkl,ijkl->',acdm.H2,AAIJ)
    ACDM_W0_energy = acdm.Hamiltonian_Energy(X0)
    W0_error = ACDM_W0_energy
    print(header+'Wavefunction ACDM Total Energy',ACDM_W0_energy )
    ACDM.info_constraints(acdm,X0,header=header+'ACDM_from_pyscf ')
    print(header+'ACDM_from_pyscf Lagrangian',acdm.Lagrangian(X0), 'gradient',max(abs(acdm.Gradient_Lagrangian(X0))))
    return X0


def percent(FCI_e,N4_e):
    """Calculate the percentage of N4 energy"""
    if FCI_e == 0.0:
        return 100.0*(1.0 - (N4_e - FCI_e))
    else:
        return 100.0*(1.0 - (N4_e - FCI_e)/abs(FCI_e))


def print_percent(header,FCI_e,N4_e):
    """Print the percentage of energies in color"""
    diff = N4_e - FCI_e
    p = percent(FCI_e,N4_e)
    print(header,'%20.16f %20.16f' % (FCI_e,N4_e),tc.colored('diff %10.3e percentage %7.5f%%' % (diff,p),'red'))


def FCI_compare_energies(acdm,X0,FCI,header='',output=False):
    """Compare the energies from FCI"""
    diff = dict()
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    AA0 = np.einsum('im,km->ik',A0,A0)
    CC0 = np.einsum('im,km->ik',C0,C0)
    AAIJ = np.einsum('ijm,klm->ikjl',AIJ,AIJ)
    FCI_e1 = np.einsum('ij,ij->',acdm.H1,FCI.rdm1)
    N4_e1 = np.einsum('ij,ij->',acdm.H1,AA0)
    diff['e1'] = abs(N4_e1-FCI_e1)
    print_percent(header+'FCI,N4 h1 energy', FCI_e1,N4_e1)
    FCI_e2 = 0.5*np.einsum('ijkl,ijkl->',acdm.H2,FCI.rdm2)
    N4_e2 = 0.5*np.einsum('ijkl,ijkl->',acdm.H2,AAIJ)
    diff['e2'] = abs(N4_e2-FCI_e2)
    print_percent(header+'FCI,N4 h2 energy', FCI_e2,N4_e2)
    N4_energy = acdm.Hamiltonian_Energy(X0)
    FCI.energy = np.einsum('ij,ij->',acdm.H1,FCI.rdm1) + 0.5*np.einsum('ijkl,ijkl->',acdm.H2,FCI.rdm2)
    diff['energy'] = abs(N4_energy-FCI.energy)
    print_percent(header+'FCI, N4 energies',FCI.energy,N4_energy)
    print(header+'Check FCI energy=e1+e2',FCI.energy-(FCI_e1+FCI_e2), 'check N4 energy=e1+e2',N4_energy-(N4_e1+N4_e2))
    diff['rdm1'] = max(abs(np.reshape(AA0 - FCI.rdm1,acdm.n2)))
    diff['rdm2'] = max(abs(np.reshape(AAIJ - FCI.rdm2,acdm.n4)))
    print(header+tc.colored('diff RDM1 %10.3e diff RDM2 %10.3e' % (diff['rdm1'],diff['rdm2']),'red'))
    if output:
        return diff


def opt_lagrangian(acdm,X0,maxiter=100,disp=True,header=''):
    """Optimization of the Lagrangian using the root findingi algorithm"""
    #Wolfe better than armijo, rdiff, very important to be small to have gradient lower than 1.e-5
    sol=scipy.optimize.root(fun=acdm.Gradient_Lagrangian, x0=X0,method='krylov',\
                          options={'disp': disp, 'xatol': 1.e-9,'xtol': 1.e-4,'fatol': 1.e-6,'maxiter': maxiter, 'line_search': 'wolfe', \
                         'jac_options': {'rdiff': 1.e-10, 'method': 'lgmres', 'inner_tol': 1.e-8}})
    print(tc.colored(header+'Message of fsolve ier=%d: %s' % (sol.status,sol.message),'magenta'))
    print(tc.colored(header+'Number of iterations %0d' % sol.nit,'magenta'))
    print(tc.colored(header+'Max Gradient values %10.4e'  % max(sol.fun),'magenta'))
    result = { 'method': 'Lagrangian', 'sol': sol, 'X0': sol.x }
    return result


def opt_penalty_method(acdm,X0,maxiter=100,disp=True,all=False,imax=7,header='',verbose=True,\
                       FCI_energy=0.0,rho=10.0,gamma=10,tau=0.5,rho_step = 2,rho_max=100000,tol=1.e-4):
    """Optimization of the penalty method using the minimization"""
    #Parameters of the algorithm
    acdm.rho = rho
    #Build the dictionary result
    result = { 'method': 'Penalty_Method','rho': rho, 'gamma': gamma, 'tau': tau, 'maxiter': maxiter, \
              'rho_step': rho_step, 'rho_max': rho_max, 'tol': tol }
    norm_CXL_previous = 1.e10
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    for i in range(maxiter):
        XP = acdm.Pack_XP(A0,C0,AIJ,CIJ)
        #1. Minimize L(XP)
        sol = scipy.optimize.minimize(acdm.Penalty_Method_XP,XP,method='CG', \
                                      jac=acdm.Gradient_Penalty_Method_XP,\
                       options={'disp': disp, 'maxiter': maxiter, 'gtol': 1.e-6 } )
        XP = sol.x
        A0,C0,AIJ,CIJ = acdm.Depack_XP(XP)
        X0 = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        result[i] = ACDM.info_all(acdm,X0,header=header+'it=%0d -- ' % i,verbose=True,\
                                  output=True,FCI_energy=FCI_energy,col=('white','white','white','white','red','white'))
        #2. Update Minimizers
        CXL = acdm.Constraints(XP)
        norm_CXL = np.linalg.norm(CXL)
        result[i]['rho'] = acdm.rho
        result[i]['norm_CXL'] = norm_CXL
        if verbose:
            print(header+'it=%0d -- ' % i, \
                  tc.colored('acdm.rho %12.6f norm_CXL %12.6e <= tau*norm_CXL_previous %12.6e (%12.6e)' % \
                         (acdm.rho,norm_CXL,tau*norm_CXL_previous,norm_CXL_previous),'blue'))
        #3. Update r
        if i == 0 or (norm_CXL <= tau * norm_CXL_previous) :
            acdm.rho = gamma*acdm.rho
        else:
            acdm.rho = acdm.rho + rho_step
        if acdm.rho > rho_max:
            acdm.rho = rho_max
        norm_CXL_previous = norm_CXL
        if norm_CXL < tol:
            #If constraints less than tol, we stop
            if verbose:
                print(header,tc.colored('stop iterations, norm_CXL %12.6e < tol %12.6e' % (norm_CXL,tol),'blue'))
            break
    result['imax'] = i
    result['X0'] = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
    return result


def opt_augmented_lagrangian(acdm,X0,maxiter=100,disp=True,imax=7,header='',verbose=True,FCI_energy=0.0,\
                             rho=1.0,gamma=1.3,tau=0.5,rho_max=100000,tol=1.e-4):
    """Optimization of the augmented Lagrangian method using minimization
    From the book practical Augmented lagrangian methods of Constrained Optimization,
    E. G. Birkin, J. M. Martinez, SIAM, p. 33"""
    Lmin = 1.0
    Lmax = 10.0
    L = 0.5*(Lmin+Lmax)
    #Parameters of the algorithm
    acdm.rho = rho
    #Build the dictionary result
    result = { 'method': 'Augmented_Lagrangian','rho': rho, 'gamma': gamma, 'tau': tau, 'maxiter': maxiter, \
              'rho_max': rho_max, 'tol': tol }
    norm_CXL_previous = 1.e10
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    for i in range(maxiter):
        XP = acdm.Pack_XP(A0,C0,AIJ,CIJ)
        XL = acdm.Pack_XL(Lambda0,muA0,muC0,LambdaIJ,muAI,muCI)
        #1. Minimize L[XL](XP)
        sol = scipy.optimize.minimize(acdm.Augmented_Lagrangian_XP,XP,args=XL,method='CG', \
                                      jac=acdm.Gradient_Augmented_Lagrangian_XP,\
                       options={'disp': disp, 'maxiter': maxiter, 'gtol': 1.e-6 } )
        XP = sol.x
        A0,C0,AIJ,CIJ = acdm.Depack_XP(XP)
        X0 = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        result[i] = ACDM.info_all(acdm,X0,header=header+'it=%0d -- ' % i,output='True',verbose=verbose,\
                             FCI_energy=FCI_energy,col=('white','white','white','red','white','white'))
        #2. Update Minimizers
        CXL = acdm.Constraints(XP)
        norm_CXL = np.linalg.norm(CXL)
        result[i]['rho'] = acdm.rho
        result[i]['norm_CXL'] = norm_CXL
        XL += np.multiply(acdm.rho,CXL)
        Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = acdm.Depack_XL(XL)
        if verbose:
            print(header+'it=%0d -- ' % i, \
                  tc.colored('acdm.rho %12.6f norm_CXL %12.6e <= tau*norm_CXL_previous %12.6e (%12.6e)' % \
                         (acdm.rho,norm_CXL,tau*norm_CXL_previous,norm_CXL_previous),'blue'))
        #3. Update r
        if i == 0 or (norm_CXL <= tau * norm_CXL_previous):
            acdm.rho = gamma*acdm.rho
        else:
            acdm.rho = acdm.rho
        if acdm.rho > rho_max:
            acdm.rho = rho_max
        norm_CXL_previous = norm_CXL
        if norm_CXL < tol:
            #If constraints less than tol, we stop
            if verbose:
                print(header,tc.colored('stop iterations, norm_CXL %12.6e < tol %12.6e' % (norm_CXL,tol),'blue'))
            break
        #5. Update l
        #At the present stage do nothing
    result['imax'] = i
    result['X0'] = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
    return result


def plot_result(result,keys):
    """Plot the result for the keys"""
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import figure
    figure(figsize=(16, 6), dpi=80)
    imax = result['imax']
    x = np.array(range(imax))
    y = np.zeros( [len(keys),imax] )
    for i in range(imax):
        for j,key in enumerate(keys):
            y[j,i] = abs(result[i][key])
    plt.yscale('log')
    for j,key in enumerate(keys):
        plt.plot(x,y[j],label=key)
    plt.legend()
    plt.show()


def update_augmented_multipliers(acdm,X0):
    """For the Augmented Lagrangian algorithm, update the Lagrange multipliers"""
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    muA0 += acdm.A0_Constraints(A0)*acdm.rho
    muC0 += acdm.C0_Constraints(C0)*acdm.rho
    Lambda0 += acdm.D0_Constraints(A0,C0)*acdm.rho
    LambdaIJ += acdm.DIJ_Constraints(AIJ,CIJ,A0,C0)*acdm.rho
    muAI += acdm.AIJ_Constraints(AIJ,A0,C0)*acdm.rho
    muCI += acdm.CIJ_Constraints(CIJ,A0,C0)*acdm.rho
    return acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)


def H_random(norb,nelec):
    """Build a random Hamiltonian"""
    #Define Hamiltonian
    h1 = -np.random.random((norb,norb))
    h2 = -np.random.random((norb,)*4)
    h1 = np.float64(0.5)*(h1 + h1.T)
    h2 = np.float64(0.5)*(h2 + h2.transpose(1,0,2,3))
    h2 = np.float64(0.5)*(h2 + h2.transpose(0,1,3,2))
    h2 = np.float64(0.5)*(h2 + h2.transpose(2,3,0,1))#To be changed as you like
    return h1,h2


def H1_into_H2(h1,h2,norb,nelec):
    """Add one-body Hamiltonian into two-body Hamiltonian"""
    #Sum of 2 terms to keep the symmetries
    h2n = h2 + np.einsum('iI,jJ-> iIjJ',h1/np.float64(nelec-1),np.eye(norb)) \
             + np.einsum('iI,jJ-> jJiI',h1/np.float64(nelec-1),np.eye(norb))
    return h2n


def H1_from_H2(h1,h2,norb,nelec):
    """Extract the maximum h1 into h2"""
    #Transform the Hamiltonian related to the AIJ_Constraints relation
    #Sum of 2 terms to keep the symmetries
    #h2n = h2 + np.einsum('iI,jJ-> iIjJ',h1/np.float64(Ns-1),np.eye(ns)) \
    #         + np.einsum('iI,jJ-> jJiI',h1/np.float64(Ns-1),np.eye(ns))
    H12 = 0.5*np.einsum('iIjj->iI',acdm.H2)/np.float64(norb)
    H1new = H1 + H12
    H2new = H2 - np.einsum('iI,jJ-> iIjJ',H12/np.float64(nelec-1),np.eye(norb)) \
               - np.einsum('iI,jJ-> jJiI',H12/np.float64(nelec-1),np.eye(norb))
    return H1new,H2new


def Hamiltonian_nospin(h1,h2,norb):
    """Transform a spin Hamiltonian into a nospin Hamiltonian"""
    h1nospin = np.zeros( (2*norb,)*2 )
    h1nospin[:norb,:norb] = h1
    h1nospin[norb:,norb:] = h1
    h2nospin = np.zeros( (2*norb,)*4 )
    h2nospin[:norb,:norb,:norb,:norb] = h2 #alpha alpha
    h2nospin[norb:,norb:,norb:,norb:] = h2 #beta  beta
    h2nospin[:norb,:norb,norb:,norb:] = h2 #alpha beta
    h2nospin[norb:,norb:,:norb,:norb] = h2 #beta  alpha
    return h1nospin,h2nospin

#Test of this module
if __name__ == '__main__':
    import numpy as np
    from scipy.special import binom  #Binom formula
    from pyscf import fci
    np.random.seed(1)
    #Calculate solution of a Hamiltonian
    norb = 4
    nelec = 2
    ndet = int(binom(norb,nelec))
    #Define Hamiltonian
    h1,h2 = H_random(norb,nelec)
    fci = FCI(h1,h2,nelec)
    W0 = Wavefunction_from_pyscf(norb,nelec,fci.fcivec)

    W0.DM_all()
    test1 = W0.positivity(verbose=False)
    eeee = len(np.where(abs(W0.eeee) < 1.e-12)[0]) - (W0.n2-W0.n2m)
    eheh = len(np.where(abs(W0.eheh) < 1.e-12)[0])
    hehe = len(np.where(abs(W0.hehe) < 1.e-12)[0])
    hhhh = len(np.where(abs(W0.hhhh) < 1.e-12)[0]) - (W0.n2-W0.n2m)
    in2m = W0.n2m - 1
    test2 = W0.DM_T1(verbose=True)
    test3 = W0.DM_T2(verbose=True)
    test = test1 and test2 and test3
    text = "n=%2d N=%2d n2=%3d eeee=%9.2e %3d eheh=%9.2e %3d hehe=%9.2e %3d hhhh=%9.2e %3d %r" % \
        (norb,nelec,W0.n2,W0.eeee[in2m],eeee, W0.eheh[0],eheh, W0.hehe[0],hehe, W0.hhhh[in2m],hhhh, test)
    print(text)
    W0.DM_check_relations(verbose=True)

    AA0 = np.einsum('im,Im->iI',W0.A0,W0.A0)
    AAIJ = np.einsum('ijm,IJm->iIjJ',W0.AIJ,W0.AIJ)
    W0_ACDM_energy = np.einsum('ij,ij->',h1,AA0)+0.5*np.einsum('ijkl,ijkl->',h2,AAIJ)
    print('W0 ACDM energy',W0_ACDM_energy,fci.energy)
    #Change the Hamiltonian to have only a two-body Hamiltonian (H1 into H2)
    H1T = h1 - h1
    H2T = h2 + np.einsum('iI,jJ-> iIjJ',h1/np.float64(W0.N-1),np.eye(W0.n)) \
             + np.einsum('iI,jJ-> jJiI',h1/np.float64(W0.N-1),np.eye(W0.n))

    #Calculate Hamiltonians for the symmetrized Lagrangian
    H1V = 0.25*np.einsum('iIjj->iI',H2T)
    H2V = 0.5*H2T
    H1F = H1V - H1V
    H2F = H2V + np.einsum('iI,jJ-> iIjJ',H1V/np.float64(W0.N-1),np.eye(W0.n)) \
              + np.einsum('iI,jJ-> jJiI',H1V/np.float64(W0.N-1),np.eye(W0.n))

    acdm = ACDM.ACDM(ns=W0.n,Ns=W0.N,h1=h1,h2=h2,method="nospin")
    #Define the Hamiltonian for the symmetrized Lagrangian
    acdm.H1A = acdm.H1C = H1F
    acdm.H2A = H2F
    acdm.H2C = H2V

    ACDM.info_acdm(acdm)
    print('-'*20,'Use the Full CI solution')
    X0 = ACDM_from_pyscf(acdm,norb,nelec,fci)
    ACDM_energy = acdm.Hamiltonian_Energy(X0)
    energy_error = ACDM_energy - fci.energy
    acdm.rho = 1000.0
    print('ACDM Total Energy         --',ACDM_energy, 'FCI energy',fci.energy, 'difference', energy_error )
    ACDM.info_energies(acdm,X0)
