#!/usr/bin/env python
# coding: utf-8

#Copyright CEA
#Author: Thierry Deutsch
#Date: 20/07/2022
# 18/07/2022 -- Remove spin and rspin version to simplify the package
# 05/07/2022 -- Change the order of packing to have a version for only parameters and parameters+lagrange multiplier
# 30/06/2022 -- Add Pack and Depack parameter components
# 31/05/2022 -- Use only one chemical potential but symetrized (option)
# 20/05/2022 -- Add information routines
# 12/05/2022 -- Add Penalty_Energy_Lagrangian (gradient easy to calculate but do not work)
# 10/05/2022 -- Add hole chemical potential and symmetrization of the Lagrangian: H*(A0-C0) - Lambda0 (A0 + C0 - 1)
# 06/05/2022 -- Use of Cholesky decomposition to build an ACDM between 2 other ACDMs with convexity
#               No always possible to keep acdm.dC0==1
# 04/05/2022 -- Use of QR decomposition
#               By convention A0 is A0[i,n] where i is states and n the projection
#               We use einsum for clarity but AA0 = np.einsum('im,Im->iI',A0,A0) correspondts to A0@A0.T
#               so for QR decomposition
#               Q,A = np.linalg.qr(A0.T); A = A.T
#               print(A0 - np.einsum('nm,im->in',Q,A))
#               AA0 = np.einsum('im,Im->iI',A0,A0); AA  = np.einsum('im,Im->iI',A,A)
#               print(AA0-AA)
# 02/05/2022 -- Use of triangular matrices for A0 and C0
# 03/02/2022 -- Correct dimensions for A0, C0, nAIJ and nCIJ to avoid too lowest energy
# 31/01/2022 -- New initial change for X0 forgetting the equality conditions considering also H2
# 28/01/2022 -- Add Symmetrized Hamiltonian
# 19/01/2022 -- Add Penalty Method and Augmented Lagrangian
# 13/01/2022 -- Reduce the limitation to N>1 (normally A0 and C0 has a line of size 1)
# 13/01/2022 -- Reduce the size of the space for AIJ and CIJ and use dAIJ -- Restrict dAIJ when few electrons
# Creation: 14/12/2021

# Class for the ACDM method
# Two-body hamiltonian and AIJ[i,j] = - AIJ[j,i]


import numpy as np
from scipy.special import binom  #Binom formula
import termcolor as tc

#Indices for triangular 2D arrays A0 and C0
def indices_2D_tri(n0,d0,p0,label):
    """Indices for the triangular 2D arrays A0 and C0"""
    I0 = [] ; I1 = []
    #print('n0,d0,p0',n0,d0,p0)
    for i in range(n0):
        for m in range( min(i+1,d0) ):
            I0.append(i); I1.append(m)
            #print( "[ %2d] length=%3d -- i=%2d m=%2d" % (i,len(I0),i,m) )
    assert len(I0) == p0,'p=%d, p0=%d, n0=%d, d0=%d: %s' % (len(I0),p0,n0,d0,label)
    return (I0,I1)


#Size of triangular of trapezoidal matrix
def size_tri(n0,d0):
    if d0 < n0:
        return (d0*(d0+1))//2 + (n0-d0)*d0 # Trapezoidal matrix
    else:
        return (d0*(d0+1))//2            # d0 = n0


#Indices for triangular 3D arrays AIJ and CIJ
def indices_3D(ind0,ind1,nIJ,dIJ,pIJ,label):
    """Indices for the triangular 3D arrays AIJ and CIJ"""
    I0 = [] ; I1 = [] ; I2 = []
    for i in range(nIJ):
        for m in range(dIJ):
            I0.append(ind0[i]); I1.append(ind1[i]); I2.append(m)
    assert len(I2) == pIJ,'p=%d, pIJ=%d, nIJ=%d, dIJ=%d: %s' % (len(I2),pIJ,nIJ,dIJ,label)
    return (I0,I1,I2)


#Indices for triangular 3D arrays AIJ and CIJ and also triangular components
def indices_3D_tri(ind0,ind1,nIJ,dIJ,pIJ,label):
    """Indices for the triangular 3D arrays AIJ and CIJ"""
    I0 = [] ; I1 = [] ; I2 = []
    #print('nIJ,dIJ,pIJ',nIJ,dIJ,pIJ)
    for i in range(nIJ):
        for m in range( min(i+1,dIJ) ):
            I0.append(ind0[i]); I1.append(ind1[i]); I2.append(m)
            #print( "[ %2d %2d ] length=%3d -- i=%2d j=%2d m=%2d" % (i,m,len(I0),ind0[i],ind1[i],m) )
    assert len(I2) == pIJ,'p=%d, pIJ=%d, nIJ=%d, dIJ=%d: %s' % (len(I2),pIJ,nIJ,dIJ,label)
    return (I0,I1,I2)


class ACDM:
    """Class to handle the ACDM object (Anti-Commuting Density Matrices) related to fermions
        - n total number of states,
        - N total number of electron,
        - Ns number of electron for a spin,
        - ns number of states without spin,"""
    def __init__(self,ns,Ns,h1,h2,method,rectangle=False,onechemical=True):
        """Initialize a ACDM object
            - Ns number of electron for a spin,
            - ns number of states without spin
            - h1 one-body Hamiltonian
            - h2 two-body Hamiltonian
            - method string should be 'nospin' or 'spin'."""
        #Number of orbitals
        self.ns = ns
        self.Ns = Ns
        #method
        if method == 'spin':
            self.n = 2*ns
            self.N = 2*Ns
            self.nelec = (Ns,Ns)
            self.ndet = int(binom(self.ns,self.nelec[0])**2)
        else:
            self.n = ns
            self.N = Ns
            self.nelec = (Ns,0)
            self.ndet = int(binom(self.ns,self.nelec[0]))
        self.ns2 = self.ns*self.ns
        self.method = method
        #Some options related to the calculation
        self.rectangle = rectangle     #Use rectangular matrices or triangular ones for A0, C0, AIJ, and CIJ
        self.onechemical = onechemical #Only one chemical potential both for electrons and holes
        self.noLambda0 = False         #To relax a ACDM condition
        self.noLambdaIJ = False        #To relax a ACDM condition
        #Dtermine the routines to use whether we use one or two chemical potentials
        if self.onechemical:
            self.Lagrangian = self.Lagrangian_onechemical
            self.Gradient_Lagrangian = self.Gradient_Lagrangian_onechemical
            self.Augmented_Lagrangian = self.Augmented_Lagrangian_onechemical
            self.Gradient_Augmented_Lagrangian = self.Gradient_Augmented_Lagrangian_onechemical
        else:
            self.Lagrangian = self.Lagrangian_twochemical
            self.Gradient_Lagrangian = self.Gradient_Lagrangian_twochemical
            self.Augmented_Lagrangian = self.Augmented_Lagrangian_twochemical
            self.Gradient_Augmented_Lagrangian = self.Gradient_Augmented_Lagrangian_twochemical
        #For penalty method and augmented lagrangian
        self.rho = 1.0
        #Parameters used to define objects
        self.n2 = self.n*self.n
        self.n4 = self.n2*self.n2
        #dA0 and dC0 is n or 1
        self.nA0 = self.n
        self.dA0 = min(self.n,int(binom(self.n,self.N-1)))
        if self.rectangle:
            self.pA0 = self.nA0*self.dA0
            self.indA0 = tuple(np.reshape(np.mgrid[0:self.nA0,0:self.dA0],(2,self.pA0)))
        else:
            self.pA0 = size_tri(self.nA0,self.dA0)
            self.indA0 = indices_2D_tri(self.nA0,self.dA0,self.pA0,'A0')
        self.nC0 = self.n
        self.dC0 = min(self.n,int(binom(self.n,self.N+1)))
        if self.rectangle:
            self.pC0 = self.nC0*self.dC0
            self.indC0 = tuple(np.reshape(np.mgrid[0:self.nC0,0:self.dC0],(2,self.pC0)))
        else:
            #Use triangular matrices
            self.pC0 = size_tri(self.nC0,self.dC0)
            self.indC0 = indices_2D_tri(self.nC0,self.dC0,self.pC0,'C0')
        self.nAIJ = (self.n*(self.n-1))//2                      #Number of AIJ[i,j]= - AIJ[j,i]
        self.indAIJ = np.triu_indices(self.n,k=1)               #Indices of AIJ[i,j] with i<j
        if self.rectangle:
            self.dAIJ = self.nAIJ
            self.pAIJ = self.nAIJ*self.dAIJ                        #Number of components of rectangular AIJ
            self.indmAIJ = indices_3D(self.indAIJ[0],self.indAIJ[1],self.nAIJ,self.dAIJ,self.pAIJ,'AIJ')
        else:
            self.dAIJ = min(int(binom(self.n,self.N-2)),self.nAIJ)  #Size of the (N-2)-body space
            self.pAIJ =  size_tri(self.nAIJ,self.dAIJ)              #Number of components of triangular AIJ
            self.indmAIJ = indices_3D_tri(self.indAIJ[0],self.indAIJ[1],self.nAIJ,self.dAIJ,self.pAIJ,'AIJ')
        self.nCIJ = self.n2                                     #Number of vectors CIJ[i,j,:]
        self.indCIJ = np.reshape(np.mgrid[0:self.n,0:self.n],(2,self.n2))
        if self.rectangle:
            self.dCIJ = self.n2                   #Better for convergence and do no alter the N-representability (as we add more states)
            self.pCIJ = self.nCIJ*self.dCIJ                         #Number of components of rectangular CIJ
        #self.indmCIJ = indices_3D(self.indCIJ[0],self.indCIJ[1],self.nCIJ,self.dCIJ,self.pCIJ,'CIJ')
        self.dCIJ = min(int(binom(self.n,self.N)),self.nCIJ)   #Size of the N-body space
        self.pCIJ = size_tri(self.nCIJ,self.dCIJ)
        self.indmCIJ = indices_3D_tri(self.indCIJ[0],self.indCIJ[1],self.nCIJ,self.dCIJ,self.pCIJ,'CIJ')
        #Build the Hamiltonian
        if method == 'spin':
            self.H1 = np.zeros([self.n,self.n])
            self.H1[:self.ns,:self.ns] = h1
            self.H1[self.ns:,self.ns:] = h1
            self.H2 = np.zeros( [self.n]*4 )
            self.H2[:self.ns,:self.ns,:self.ns,:self.ns] = h2 #Alpha Alpha
            self.H2[:self.ns,:self.ns,self.ns:,self.ns:] = h2 #Alpha Beta
            self.H2[self.ns:,self.ns:,self.ns:,self.ns:] = h2 #Beta Beta
            self.H2[self.ns:,self.ns:,:self.ns,:self.ns] = h2 #Beta Alpha
        else:
            self.H1 = h1.copy()
            self.H2 = h2.copy()
        #For symmetrization of the expression of the Hamiltonian Energy
        self.H2S = 0.5*self.H2
        self.H1S = self.H1 + 0.25*np.einsum('iIjj->iI',self.H2) # H2S (AAIJ - CCIJ) + H2S*AA0 via AAIJ+CCIJ=AA0*np.eye(n)
        self.H1S = 0.5*self.H1S # H1S (AA0 - CC0) + Tr(H1S) via AA0+CC0=1 and H1S AA0 = 0.5H1S(AA0-CC0)+0.5TH1S
        self.TH1S = np.einsum('ii->',self.H1S)
        # Restore permutation symmetry (normally not useful)
        self.H1 = 0.5*(self.H1 + self.H1.T)
        self.H2 = 0.5*(self.H2 + self.H2.transpose(1,0,2,3))
        self.H2 = 0.5*(self.H2 + self.H2.transpose(0,1,3,2))
        self.H2 = 0.5*(self.H2 + self.H2.transpose(2,3,0,1))
        # For the symmetrized version
        self.H1A = self.H1C = self.H1
        self.H2A = self.H2C = self.H2
        
    #------ no spin part

    #Size of X (to use scipy.optimize.root, we need a vector)
    def Size_X(self,all=True):
        """Give the size of the 1D-array to use for optimization"""
        n0 = self.pA0 + self.pC0 + self.pAIJ + self.pCIJ #A0[n,n] C0[n,n] AIJ[n,n,Am] CIJ[n,n,Cm] -- Convention of the paper i,i',j,j'
        if all:
            n0 += self.n2 + 1 + 1             #Lambda0[n,n] muA0 muC0
            n0 += self.n4 + self.n2 + self.n2 #LambdaIJ[n,n,n,n] muAI[n,n], muCI[n,n]
        return n0

    def Indice_AIJ(self):
        return self.pA0 + self.pC0

    def Indice_CIJ(self):
        return self.pA0 + self.pC0 + self.pAIJ

    def Indice_Lambda0(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ

    def Indice_muA0(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ + self.n2

    def Indice_muC0(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ + self.n2 + 1

    def Indice_LambdaIJ(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ + self.n2 + 2 

    def Indice_muAI(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ + self.n2 + 2 + self.n4

    def Indice_muCI(self):
        return self.pA0 + self.pC0 + self.pAIJ + self.pCIJ + self.n2 + 2 + self.n4 + self.n2

    #Return label and postion for testing the gradient
    def label(self,all=True):
        """Return labels and the position to test the gradient"""
        if all:
            text = [ 'A0','C0','AIJ','CIJ','Lambda0','muA0','muC0','LambdaIJ','muAI','muCI']
            pos = [ 0, self.pA0, self.Indice_AIJ(), self.Indice_CIJ(), self.Indice_Lambda0(), self.Indice_muA0(), self.Indice_muC0(), 
               self.Indice_LambdaIJ(), self.Indice_muAI(), self.Indice_muCI() ]
        else:
            text = [ 'A0','C0','AIJ','CIJ']
            pos = [ 0, self.pA0, self.Indice_AIJ(), self.Indice_CIJ() ]
        return text,pos

    #Pack A0, C0, AIJ, CIJ, Lambda0, muA0, muC0, LambdaIJ and muI into X
    def Pack_X(self,A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI,all=True):
        """Pack all components into a 1D array
           only=True for only parameters"""
        X = np.zeros(self.Size_X())
        X[0:self.pA0] = A0[self.indA0]
        n0 = self.pA0
        X[n0:n0+self.pC0] = C0[self.indC0]
        n0 = self.Indice_AIJ()
        X[n0:n0+self.pAIJ] = AIJ[self.indmAIJ]
        n0 = self.Indice_CIJ()
        X[n0:n0+self.pCIJ] = CIJ[self.indmCIJ]
        if all:
            n0 = self.Indice_Lambda0()
            if self.noLambda0:
                X[n0:n0+self.n2] = np.zeros(self.n2)
            else:
                X[n0:n0+self.n2] = np.reshape(Lambda0,self.n2)
            n0 = self.Indice_muA0()
            X[n0] = muA0
            n0 = self.Indice_muC0()
            X[n0] = muC0
            n0 = self.Indice_LambdaIJ()
            if self.noLambdaIJ:
                X[n0:n0+self.n4] = np.zeros(self.n4)
            else:
                X[n0:n0+self.n4] = np.reshape(LambdaIJ,self.n4)
            n0 = self.Indice_muAI()
            X[n0:n0+self.n2] = np.reshape(muAI,self.n2)
            n0 = self.Indice_muCI()
            X[n0:n0+self.n2] = np.reshape(muCI,self.n2)
        return X


    #Depack X into A0, C0, Lambda0, muA0, muC0, AIJ, CIJ, LambdaIJ, muAI, and muCI
    def Depack_X(self,X,all=True):
        """Depack all components from a 1D array"""
        A0 = np.zeros([self.nA0,self.dA0])
        A0[self.indA0] = X[0:self.pA0]
        n0 = self.pA0
        C0 = np.zeros([self.nC0,self.dC0])
        C0[self.indC0] = X[n0:n0+self.pC0]
        n0 = self.Indice_AIJ()
        AIJ = np.zeros([self.n,self.n,self.dAIJ])
        AIJ[self.indmAIJ] = X[n0:n0+self.pAIJ]
        AIJ += - np.einsum('ijm->jim',AIJ)
        n0 = self.Indice_CIJ()
        CIJ = np.zeros([self.n,self.n,self.dCIJ])
        CIJ[self.indmCIJ] = X[n0:n0+self.pCIJ]
        #Set to zero
        Lambda0 = np.zeros( [self.n,]*2 )
        muA0 = 0.0
        muC0 = 0.0
        LambdaIJ = np.zeros( [self.n]*4 )
        muAI = np.zeros( [self.n,]*2 )
        muCI = np.zeros( [self.n,]*2 )
        if all:
            n0 = self.Indice_Lambda0()
            if not self.noLambda0:
                Lambda0 = np.reshape(X[n0:n0+self.n2],[self.n,self.n])
            n0 = self.Indice_muA0()
            muA0 = X[n0]
            n0 = self.Indice_muC0()
            muC0 = X[n0]
            n0 = self.Indice_LambdaIJ()
            if not self.noLambdaIJ:
                LambdaIJ = np.reshape(X[n0:n0+self.n4],[self.n]*4)
            n0 = self.Indice_muAI()
            muAI = np.reshape(X[n0:n0+self.n2],[self.n,self.n])
            n0 = self.Indice_muCI()
            muCI = np.reshape(X[n0:n0+self.n2],[self.n,self.n])
        return A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI


    #Pack A0, C0, AIJ, CIJ into XP
    def Pack_XP(self,A0,C0,AIJ,CIJ):
        """Pack all parameter components into a 1D array"""
        X = np.zeros(self.Size_X(all=False))
        X[0:self.pA0] = A0[self.indA0]
        n0 = self.pA0
        X[n0:n0+self.pC0] = C0[self.indC0]
        n0 += self.pC0
        X[n0:n0+self.pAIJ] = AIJ[self.indmAIJ]
        n0 += self.pAIJ
        X[n0:n0+self.pCIJ] = CIJ[self.indmCIJ]
        return X


    #Depack XP into A0, C0, AIJ, CIJ
    def Depack_XP(self,X):
        """Depack all parameter components from a 1D array"""
        A0 = np.zeros([self.nA0,self.dA0])
        A0[self.indA0] = X[0:self.pA0]
        n0 = self.pA0
        C0 = np.zeros([self.nC0,self.dC0])
        C0[self.indC0] = X[n0:n0+self.pC0]
        n0 += self.pC0
        AIJ = np.zeros([self.n,self.n,self.dAIJ])
        AIJ[self.indmAIJ] = X[n0:n0+self.pAIJ]
        AIJ += - np.einsum('ijm->jim',AIJ)
        n0 += self.pAIJ
        CIJ = np.zeros([self.n,self.n,self.dCIJ])
        CIJ[self.indmCIJ] = X[n0:n0+self.pCIJ]
        return A0,C0,AIJ,CIJ


    #Pack XL Lagrange multipliers
    def Pack_XL(self,Lambda0,muA0,muC0,LambdaIJ,muAI,muCI):
        """Build a serial array for Lagrange multipliers"""
        nXL = self.n2 + 1 + 1 + self.n4 + self.n2 + self.n2
        XL = np.zeros ( nXL )
        XL[:self.n2] = np.reshape(Lambda0,self.n2)
        XL[self.n2] = muA0
        XL[self.n2 + 1] = muC0
        n0 = self.n2 + 2
        XL[n0:n0+self.n4] = np.reshape(LambdaIJ,self.n4)
        n0 += self.n4
        XL[n0:n0+self.n2] = np.reshape(muAI,self.n2)
        n0 += self.n2
        XL[n0:n0+self.n2] = np.reshape(muCI,self.n2)
        return XL


    #Depack XL Lagrange multipliers
    def Depack_XL(self,XL):
        """Depack serial array of the Lagrange multipliers"""
        Lambda0 = np.reshape(XL[:self.n2],[self.n,self.n])
        muA0 = XL[self.n2]
        muC0 = XL[self.n2 + 1]
        n0 = self.n2 + 2
        LambdaIJ = np.reshape(XL[n0:n0+self.n4],[self.n,]*4)
        n0 += self.n4
        muAI = np.reshape(XL[n0:n0+self.n2],[self.n,self.n])
        n0 += self.n2
        muCI = np.reshape(XL[n0:n0+self.n2],[self.n,self.n])
        return Lambda0,muA0,muC0,LambdaIJ,muAI,muCI


    #Initial Guess using H2 as H1 selecting the N(N-1)/2 lowest eigenvectors
    def Initial_X2(self):
        """Initialize a state calculating the pseudo-Slater determinant of pairs from the two-body Hamiltonian"""
        #Same as one-body but with two-body and N(N-1)/2 electrons
        Nb2 = (self.N*(self.N-1))//2
        nb2 = (self.n*(self.n-1))//2
        #Build a one-body Hamiltonian from H2 + H1 (put the max of H2 into H1)
        H2 = self.H2 + np.einsum('iI,jJ-> iIjJ',self.H1/np.float64(self.N-1),np.eye(self.n)) \
                     + np.einsum('iI,jJ-> jJiI',self.H1/np.float64(self.N-1),np.eye(self.n))
        #Then we reorder ijIJ and we consider only nb2 pairs
        H2n = np.zeros( (nb2,nb2) )
        ind = np.triu_indices(self.n,k=1)
        ind = [list(a) for a in zip(ind[0],ind[1])]
        for l,(i,j) in enumerate(ind):
            for L,(I,J) in enumerate(ind):
                H2n[l,L] = H2[i,I,j,J]
        L2,U2 = np.linalg.eigh(H2n)
        print('Initial_X2: L2=',L2)
        print('Initial_X2: sum(L2[:Nb2])',np.einsum('i->',L2[:Nb2]))
        #First AIJ, CIJ, and LambdaIJ
        L_Lambda2 = np.zeros(nb2)
        A2 = np.zeros( [nb2,self.dAIJ] )
        for l in range(Nb2):
            A2[l,l] = 1.0 #N(N-1)/2 occupation number to 1
            L_Lambda2[l] = L2[l]
        A2 = np.einsum('ij,jm->im',U2,A2)
        AIJ = np.zeros( (self.n,self.n,self.dAIJ))
        for l,(i,j) in enumerate(ind):
            AIJ[i,j,:] =  A2[l,:]
            AIJ[j,i,:] = -A2[l,:]
        #Lambda0
        Lambda2 = U2@np.diag(L_Lambda2)@U2.T
        LambdaIJ = np.zeros( (self.n,)*4 )
        for l,(i,j) in enumerate(ind):
            for L,(I,J) in enumerate(ind):
                LambdaIJ[i,I,j,J] = Lambda2[l,L]
        #Then A0, C0 from sum_j Aij Ai'j = (N-1) A0i.A0i'
        AA0 = np.einsum('ijm,Ijm->iI',AIJ,AIJ)/np.float64(self.N-1)
        LA0,U0 = np.linalg.eigh(AA0)
        print('Initial_X2: A0 L0=',LA0)
        A0 = np.einsum('ij,jm->im',U0,np.diag(LA0))
        LC0 = np.sqrt(1.0 - LA0**2)
        #Remove nan
        LC0[np.isnan(LC0)] = 0.
        print('Initial_X2: C0 L0=',LC0)
        #self.dC0 is only 1 or n
        C0 = np.einsum('ij,jm->im',U0,np.diag(LC0))
        #Finally CIJ via AIJ.AIJ + CIJ.CIJ = A0i.A0i' Djj'
        CCIJ = np.einsum('iI,jJ->iIjJ',AA0,np.eye(self.n)) - np.einsum('ijm,IJm->iIjJ',AIJ,AIJ)
        CCIJ = np.reshape( np.einsum('iIjJ->ijIJ',CCIJ),(self.n2,self.n2) )
        L2,U2 = np.linalg.eigh(CCIJ)
        CIJ = np.reshape( np.einsum('ij,jm->im',U2,np.diag(L2)), (self.n,self.n,self.n2) )
        muA0 = 0.0
        muC0 = 0.0
        Lambda0 = np.zeros([self.n,self.n])
        muAI = np.zeros([self.n,self.n])
        muCI = np.zeros([self.n,self.n])
        return self.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)

    
    #Initial Guess based on H1 and H2 
    def Initial_X(self,header=''):
        """Initialize a state calculating the Slater determinant from a one-body Hamiltonian"""
        #Build a one-body Hamiltonian from H2 + H1 (put the max of H2 into H1)
        H1 = self.H1 + 0.5*np.einsum('iIjj->iI',self.H2)/np.float64(self.n)
        L,U = np.linalg.eigh(H1)
        #----------- We define all in the basis U^{-1}
        #-- First A0, C0, Lambda0 and mu
        muA0 = L[self.N-1]
        muC0 = -L[self.N]
        L_A0 = np.zeros(self.n)
        L_C0 = np.zeros(self.n)
        L_Lambda0 = np.zeros(self.n)
        for i in range(self.N):
            L_A0[i] = 1.0 #N occupation number to 1
            L_Lambda0[i] = L[i] - muA0
        for i in range(self.N,self.n):
            L_C0[i] = 1.0 #n-N hole to one
            L_Lambda0[i] = -L[i] - muC0
        A0 = np.diag(L_A0)
        if self.dC0 == 1:
            C0 = np.reshape(L_C0, (self.nC0,self.dC0) )
        else:
            C0 = np.diag(L_C0)
        #-- Then AIJ, CIJ, LambdaIJ, muI
        AIJ = np.zeros( [self.n,self.n,self.dAIJ] )
        l = 0
        for i in range(self.N):  #AIJ[i,i,:] = 0
            for j in range(i+1,self.N):
                AIJ[i,j,l] = 1.0
                AIJ[j,i,l] = -1.0
                l += 1
        #-- CIJ matrix
        CIJ = np.zeros( [self.n,self.n,self.dCIJ] )
        l = 0
        for i in range(self.N):
            #a+iai gives the same wavefunction 
            CIJ[i,i,0] = 1.0
            for j in range(self.N,self.n):
                l += 1
                CIJ[i,j,min(l,self.dCIJ-1)] = 1.0 
        #---------- Then replace in the basis U
        A0 = np.einsum('ij,jm->im',U,A0)
        C0 = np.einsum('ij,jm->im',U,C0)
        Lambda0 = U@np.diag(L_Lambda0)@U.T
        AIJ = np.einsum('ki,lj,ijm->klm',U,U,AIJ)
        CIJ = np.einsum('ki,lj,ijm->klm',U,U,CIJ)
        muAI = np.zeros([self.n,self.n]) #Idem
        muCI = np.zeros([self.n,self.n]) #Idem
        LambdaIJ = self.H2 - np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n))
        #print(header+'A0_Constraints',np.linalg.norm(self.A0_Constraints(A0)))
        #print(header+'C0_Constraints',np.linalg.norm(self.C0_Constraints(C0)))
        #print(header+'D0_Constraints',np.linalg.norm(self.D0_Constraints(A0,C0)))
        #print(header+'AIJ_Constraints',np.linalg.norm(self.AIJ_Constraints(AIJ,A0,C0)))
        #print(header+'CIJ_Constraints',np.linalg.norm(self.CIJ_Constraints(CIJ,A0,C0)))
        #print(header+'DIJ_Constraints',np.linalg.norm(self.DIJ_Constraints(AIJ,CIJ,A0,C0)))
        #We can't use info_constraints because Pach_X compact properly only triangular matrices
        #---------- Check conditions
        #--------- QR decomposition to have a lower triangular matrix
        A0,C0,AIJ,CIJ = QR_decomposition(self,A0,C0,AIJ,CIJ)
        X0 = self.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        info_constraints(self,X0,header=header+'Initial_X -- QR -- ')
        return X0


    #--- General part based on Depack_X and Pack_X ---
    #Constraints for A0
    def A0_Constraints(self,A0):
        """Normalization of A0 to keep the number of electrons N"""
        return (np.einsum('im,im->',A0,A0) - np.float64(self.N))
    def Gradient_A0_Constraints(self,A0,muA0=1.0):
        """Gradients of A0_Constraints"""
        HH = muA0*np.eye(self.n)
        GA0 = np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        return GA0

    #Constraints for C0
    def C0_Constraints(self,C0):
        """Normalization of C0 to keep the number of holes n-N"""
        return (np.einsum('im,im->',C0,C0) - np.float64(self.n-self.N))
    def Gradient_C0_Constraints(self,C0,muC0=1.0):
        """Gradients of C0_Constraints"""
        HH = muC0*np.eye(self.n)
        GC0 = np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)        
        return GC0

    #Constraints for AC0
    def AC0_Constraints(self,A0,C0):
        """Normalization of A0-C0 to keep the number of electrons N"""
        return np.einsum('im,im->',A0,A0) - np.einsum('im,im->',C0,C0) - np.float64( (2*self.N - self.n) )
    def Gradient_AC0_Constraints(self,A0,C0,muA0=1.0):
        """Gradients of A0_Constraints"""
        HH = muA0*np.eye(self.n)
        GA0 =   np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        GC0 = - np.einsum('ij,jm->im',HH,C0) - np.einsum('ij,im->jm',HH,C0)
        return GA0,GC0
    
    #Constraints for D0
    def D0_Constraints(self,A0,C0):
        """Anti-commuting constraint"""
        return np.einsum('im,Im->iI',A0,A0) + np.einsum('im,Im->Ii',C0,C0) - np.eye(self.n)
    def Gradient_D0_Constraints(self,A0,C0,Lambda0):
        """Gradients of anti-commuting constraints"""
        HH = Lambda0
        GA0 = np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        GC0 = np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        return GA0,GC0

    #Constraints for AIJ
    def AIJ_Constraints(self,AIJ,A0,C0):
        """Normalization of AIJ to keep the number of electrons N-1 <A0i,A0i'>"""
        #return np.einsum('ijm,Ijm->iI',AIJ,AIJ) - np.float64(self.N-1)*np.einsum('im,Im->iI',A0,A0)
        AC0 = 0.5*( np.einsum('im,Im->iI',A0,A0) - np.einsum('im,Im->Ii',C0,C0) + np.eye(self.n) )
        return np.einsum('ijm,Ijm->iI',AIJ,AIJ) - np.float64(self.N-1)*AC0
    def Gradient_AIJ_Constraints(self,AIJ,A0,C0,muAI):
        """Gradients of AIJ_Constraints"""
        HH = 0.5*np.float64(self.N-1)*muAI
        GA0 = - np.einsum('ij,jm->im',HH,A0) - np.einsum('ij,im->jm',HH,A0)
        GC0 =   np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        HH = np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n))
        GAIJ = np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return GAIJ,GA0,GC0

    #Constraints for CIJ
    def CIJ_Constraints(self,CIJ,A0,C0):
        """Normalization of AIJ to keep the number of holes (n-(N-1)) <A0i,A0i'>"""
        #return np.einsum('ijm,Ijm->iI',CIJ,CIJ) - np.float64(self.n-(self.N-1))*np.einsum('im,Im->iI',A0,A0)
        AC0 = 0.5*( np.einsum('im,Im->iI',A0,A0) - np.einsum('im,Im->Ii',C0,C0) + np.eye(self.n) )
        return np.einsum('ijm,Ijm->iI',CIJ,CIJ) - np.float64(self.n-(self.N-1))*AC0
    def Gradient_CIJ_Constraints(self,CIJ,A0,C0,muCI):
        """Gradients of AIJ_Constraints"""
        HH = 0.5*np.float64(self.n-(self.N-1))*muCI
        GA0 = - np.einsum('ij,jm->im',HH,A0) - np.einsum('ij,im->jm',HH,A0)
        GC0 =   np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        HH = np.einsum('iI,jJ->iIjJ',muCI,np.eye(self.n))
        GCIJ = np.einsum('iIjJ,Ijm->iJm',HH,CIJ) + np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        return GCIJ,GA0,GC0

    def ACIJ_Constraints(self,AIJ,CIJ,A0,C0):
        """Normalization of Tr(AIJ.AIJ)-Tr(CIJ.CIJ) to keep the number of electrons (2N-n) <A0i,A0i'>"""
        #return np.einsum('ijm,Ijm->iI',AIJ,AIJ) - np.float64(self.N-1)*np.einsum('im,Im->iI',A0,A0)
        AC0 = 0.5*( np.einsum('im,Im->iI',A0,A0) - np.einsum('im,Im->Ii',C0,C0) + np.eye(self.n) )
        return np.einsum('ijm,Ijm->iI',AIJ,AIJ) - np.einsum('ijm,Ijm->iI',CIJ,CIJ) - np.float64( 2*(self.N-1) - self.n )*AC0
    def Gradient_ACIJ_Constraints(self,AIJ,CIJ,A0,C0,muAI):
        """Gradients of ACIJ_Constraints"""
        HH = 0.5*np.float64(2*(self.N-1)-self.n)*muAI
        GA0 = - np.einsum('ij,jm->im',HH,A0) - np.einsum('ij,im->jm',HH,A0)
        GC0 =   np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        HH = np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n))
        GAIJ =   np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        GCIJ = - np.einsum('iIjJ,IJm->ijm',HH,CIJ) - np.einsum('iIjJ,ijm->IJm',HH,CIJ)
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return GAIJ,GCIJ,GA0,GC0

    #Constraints for DIJ
    def DIJ_Constraints(self,AIJ,CIJ,A0,C0):
        """Anti-commuting constraints over the DI"""
        #return np.einsum('ijm,IJm->iIjJ',AIJ,AIJ) + np.einsum('ijm,IJm->iIJj',CIJ,CIJ) - np.einsum('im,Im,jJ->iIjJ',A0,A0, np.eye(self.n))
        AC0 = 0.5*( np.einsum('im,Im->iI',A0,A0) - np.einsum('im,Im->Ii',C0,C0) + np.eye(self.n) )
        return np.einsum('ijm,IJm->iIjJ',AIJ,AIJ) + np.einsum('ijm,IJm->iIJj',CIJ,CIJ) - np.einsum('iI,jJ->iIjJ',AC0, np.eye(self.n))
    def Gradient_DIJ_Constraints(self,AIJ,CIJ,A0,C0,LambdaIJ):
        """Gradients of DIJ_Constraints"""
        HH = 0.5*np.einsum('iIjj->iI',LambdaIJ)
        GA0 = - np.einsum('ij,jm->im',HH,A0) - np.einsum('ij,im->jm',HH,A0)
        GC0 =   np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        HH = LambdaIJ
        GAIJ = np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        GCIJ = np.einsum('iIjJ,Ijm->iJm',HH,CIJ) + np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        return GAIJ,GCIJ,GA0,GC0

    #Arrays of constraints
    def Constraints(self,XP):
        """Calculate Constraints from XP for update in the augmented Lagrangian algorithm"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        CLambda0 = self.D0_Constraints(A0,C0)
        CmuA0 = self.A0_Constraints(A0)
        CmuC0 = self.C0_Constraints(C0)
        CLambdaIJ = self.DIJ_Constraints(AIJ,CIJ,A0,C0)
        CmuAI = self.AIJ_Constraints(AIJ,A0,C0)
        CmuCI = self.CIJ_Constraints(CIJ,A0,C0)
        return self.Pack_XL(CLambda0,CmuA0,CmuC0,CLambdaIJ,CmuAI,CmuCI)


    #Total energy of the system
    def Simple_Energy(self,X):
        """Total energy, version non symmetrized"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return np.einsum('iI,im,Im->',self.H1,A0,A0) + 0.5*np.einsum('iIjJ,ijm,IJm->',self.H2,AIJ,AIJ)


    #Gradient of Energy
    def Gradient_Simple_Energy(self,X):
        """Gradient of the total energy without constraint, version non symmetrized"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        HH = self.H1
        GA0 =   np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        GC0 = np.zeros_like(C0)
        GLambda0 = np.zeros( (self.n,)*2 )
        GmuA0 = 0.0
        GmuC0 = 0.0
        HH = 0.5*self.H2
        GAIJ = np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        GCIJ = np.zeros_like(CIJ)
        GLambdaIJ = np.zeros( (self.n,)*4 )
        GmuAI = np.zeros( (self.n,)*2 )
        GmuCI = np.zeros( (self.n,)*2 )
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)


    def Hamiltonian_Energy(self,X):
        """Total energy"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.TH1S + np.einsum('iI,im,Im->',self.H1S,A0,A0) - np.einsum('iI,im,Im->',self.H1S,C0,C0) \
                + 0.5*( np.einsum('iIjJ,ijm,IJm->',self.H2S,AIJ,AIJ) - np.einsum('iIjJ,iJm,Ijm->',self.H2S,CIJ,CIJ) )

    #Gradient of Energy
    def Gradient_Energy(self,X):
        """Gradient of the total energy without constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        HH = self.H1S
        GA0 =   np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        GC0 = - np.einsum('ij,jm->im',HH,C0) - np.einsum('ij,im->jm',HH,C0)
        GLambda0 = np.zeros( (self.n,)*2 )
        GmuA0 = 0.0
        GmuC0 = 0.0
        HH = 0.5*self.H2S
        GAIJ =   np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        GCIJ = - np.einsum('iIjJ,Ijm->iJm',HH,CIJ) - np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        GLambdaIJ = np.zeros( (self.n,)*4 )
        GmuAI = np.zeros( (self.n,)*2 )
        GmuCI = np.zeros( (self.n,)*2 )
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)

    #Lagrangian
    def Lagrangian_twochemical(self,X):
        """Lagrangian of the system with Lagrange multiplier to consider the constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.TH1S + np.einsum('iI,im,Im->',self.H1S,A0,A0) - np.einsum('iI,im,Im->',self.H1S,C0,C0) \
            + 0.5*( np.einsum('iIjJ,ijm,IJm->',self.H2S,AIJ,AIJ) - np.einsum('iIjJ,iJm,Ijm->',self.H2S,CIJ,CIJ) ) \
            - muA0 * self.A0_Constraints(A0) - muC0 * self.C0_Constraints(C0) - np.einsum('iI,iI->',Lambda0,self.D0_Constraints(A0,C0)) \
               - np.einsum('iI,iI->', muAI, self.AIJ_Constraints(AIJ,A0,C0)) - np.einsum('iI,iI->', muCI, self.CIJ_Constraints(CIJ,A0,C0)) \
               - np.einsum('iIjJ,iIjJ->',LambdaIJ,self.DIJ_Constraints(AIJ,CIJ,A0,C0))

    #Gradient of Lagrangian
    def Gradient_Lagrangian_twochemical(self,X):
        """Gradient of the Lagrangian"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        H = self.H1S + 0.5*( np.float64(self.N-1)*muAI + np.float64(self.n-(self.N-1))*muCI + np.einsum('iIjj->iI',LambdaIJ) )
        HH =   H - Lambda0 - muA0*np.eye(self.n)
        GA0 = np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        HH = - H - Lambda0 - muC0*np.eye(self.n)
        GC0 = np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        GLambda0 = - self.D0_Constraints(A0,C0)
        GmuA0 = - self.A0_Constraints(A0)
        GmuC0 = - self.C0_Constraints(C0)
        HH =   0.5*self.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n))
        GAIJ = np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        HH = - 0.5*self.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muCI,np.eye(self.n)) #Ordering with j and J for muCI?
        GCIJ = np.einsum('iIjJ,Ijm->iJm',HH,CIJ) + np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        GLambdaIJ = - self.DIJ_Constraints(AIJ,CIJ,A0,C0)
        GmuAI = - self.AIJ_Constraints(AIJ,A0,C0)
        GmuCI = - self.CIJ_Constraints(CIJ,A0,C0)
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)
    

    def Lagrangian_onechemical(self,X):
        """Lagrangian of the system with Lagrange multiplier to consider the constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.TH1S + np.einsum('iI,im,Im->',self.H1S,A0,A0) - np.einsum('iI,im,Im->',self.H1S,C0,C0) \
            + 0.5*( np.einsum('iIjJ,ijm,IJm->',self.H2S,AIJ,AIJ) - np.einsum('iIjJ,iJm,Ijm->',self.H2S,CIJ,CIJ) ) \
            - muA0 * self.AC0_Constraints(A0,C0) - np.einsum('iI,iI->',Lambda0,self.D0_Constraints(A0,C0)) \
               - np.einsum('iI,iI->', muAI, self.ACIJ_Constraints(AIJ,CIJ,A0,C0)) \
               - np.einsum('iIjJ,iIjJ->',LambdaIJ,self.DIJ_Constraints(AIJ,CIJ,A0,C0))

    #Gradient of Lagrangian
    def Gradient_Lagrangian_onechemical(self,X):
        """Gradient of the Lagrangian"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        H = self.H1S + 0.5*( np.float64(2*(self.N-1)-self.n)*muAI + np.einsum('iIjj->iI',LambdaIJ) )
        HH =   H - Lambda0 - muA0*np.eye(self.n)
        GA0 = np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        HH = - H - Lambda0 + muA0*np.eye(self.n)
        GC0 = np.einsum('ij,jm->im',HH,C0) + np.einsum('ij,im->jm',HH,C0)
        GLambda0 = - self.D0_Constraints(A0,C0)
        GmuA0 = - self.AC0_Constraints(A0,C0)
        GmuC0 = 0.0
        HH =   0.5*self.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n))
        GAIJ = np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        HH = - 0.5*self.H2S - LambdaIJ + np.einsum('iI,jJ->iIjJ',muAI,np.eye(self.n)) #Ordering with j and J for muCI?
        GCIJ = np.einsum('iIjJ,Ijm->iJm',HH,CIJ) + np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        GLambdaIJ = - self.DIJ_Constraints(AIJ,CIJ,A0,C0)
        GmuAI = - self.ACIJ_Constraints(AIJ,CIJ,A0,C0)
        GmuCI = np.zeros( [self.n,]*2 )
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)
    

    #Used Hamiltonian_Energy + 0.5*rho*Lagrangian**2 (easier to calculate the gradient but move the minimum...)
    def Penalty_Energy_Lagrangian(self,X):
        """Special penalty function Hamiltonian_Energy + 0.5*rho*Lagrangian"""
        return self.Hamiltonian_Energy(X) + 0.5*self.rho * self.Lagrangian(X)**2

    #Gradient for the Penalty Energy Lagrangian
    def Gradient_Penalty_Energy_Lagrangian(self,X):
        """Gradient of the penalty energy function"""
        return self.Gradient_Energy(X) + self.rho*self.Lagrangian(X)*self.Gradient_Lagrangian(X)

    #Penalty Method -- the right one    
    def Penalty_Method(self,X):
        """Penalty function adding to the total energy the square of the constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.Hamiltonian_Energy(X) \
            + 0.5*self.rho * ( self.A0_Constraints(A0)**2 + self.C0_Constraints(C0)**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.AIJ_Constraints(AIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.CIJ_Constraints(CIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0), self.n2*self.n2 )**2 ) )

    #Gradient for the Penalty Method
    def Gradient_Penalty_Method(self,X):
        """Gradient of the penalty function"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        GX = self.Gradient_Energy(X)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        GA0 += self.rho * self.A0_Constraints(A0) * self.Gradient_A0_Constraints(A0)
        GC0 += self.rho * self.C0_Constraints(C0) * self.Gradient_C0_Constraints(C0)
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.D0_Constraints(A0,C0))
        GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gA0,gC0 = self.Gradient_AIJ_Constraints(AIJ,A0,C0,muAI=self.AIJ_Constraints(AIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gCIJ,gA0,gC0 = self.Gradient_CIJ_Constraints(CIJ,A0,C0,muCI=self.CIJ_Constraints(CIJ,A0,C0))
        GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0,LambdaIJ=self.DIJ_Constraints(AIJ,CIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        GmuA0 = 0.0
        GmuC0 = 0.0
        GLambda0 = np.zeros( (self.n,self.n) )
        GLambdaIJ = np.zeros( (self.n,)*4 )
        GmuAI = np.zeros( (self.n,)*2 )
        GmuCI = np.zeros( (self.n,)*2 )
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)

    #Penalty with Square of Energy
    def Penalty_Square(self,X):
        """Penalty function adding to the total energy the square of the constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return 0.5*self.Hamiltonian_Energy(X)**2 \
            + 0.5*self.rho * ( self.A0_Constraints(A0)**2 + self.C0_Constraints(C0)**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.AIJ_Constraints(AIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.CIJ_Constraints(CIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0), self.n2*self.n2 )**2 ) )

    #Gradient for the Penalty Method
    def Gradient_Penalty_Square(self,X):
        """Gradient of the penalty function"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        GX = self.Hamiltonian_Energy(X)*self.Gradient_Energy(X)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        GA0 += self.rho * self.A0_Constraints(A0) * self.Gradient_A0_Constraints(A0)
        GC0 += self.rho * self.C0_Constraints(C0) * self.Gradient_C0_Constraints(C0)
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.D0_Constraints(A0,C0))
        GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gA0,gC0 = self.Gradient_AIJ_Constraints(AIJ,A0,C0,muAI=self.AIJ_Constraints(AIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gCIJ,gA0,gC0 = self.Gradient_CIJ_Constraints(CIJ,A0,C0,muCI=self.CIJ_Constraints(CIJ,A0,C0))
        GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0,LambdaIJ=self.DIJ_Constraints(AIJ,CIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        GmuA0 = 0.0
        GmuC0 = 0.0
        GLambda0 = np.zeros( (self.n,self.n) )
        GLambdaIJ = np.zeros( (self.n,)*4 )
        GmuAI = np.zeros( (self.n,)*2 )
        GmuCI = np.zeros( (self.n,)*2 )
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)

    #Penalty Method -- the right one    
    def Penalty_Method(self,X):
        """Penalty function adding to the total energy the square of the constraints"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.Hamiltonian_Energy(X) \
            + 0.5*self.rho * ( self.A0_Constraints(A0)**2 + self.C0_Constraints(C0)**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.AIJ_Constraints(AIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.CIJ_Constraints(CIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0), self.n2*self.n2 )**2 ) )

    #Gradient for the Penalty Method
    def Gradient_Penalty_Method(self,X):
        """Gradient of the penalty function"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        GX = self.Gradient_Energy(X)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        GA0 += self.rho * self.A0_Constraints(A0) * self.Gradient_A0_Constraints(A0)
        GC0 += self.rho * self.C0_Constraints(C0) * self.Gradient_C0_Constraints(C0)
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.D0_Constraints(A0,C0))
        GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gA0,gC0 = self.Gradient_AIJ_Constraints(AIJ,A0,C0,muAI=self.AIJ_Constraints(AIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gCIJ,gA0,gC0 = self.Gradient_CIJ_Constraints(CIJ,A0,C0,muCI=self.CIJ_Constraints(CIJ,A0,C0))
        GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0,LambdaIJ=self.DIJ_Constraints(AIJ,CIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        GmuA0 = 0.0
        GmuC0 = 0.0
        GLambda0 = np.zeros( (self.n,self.n) )
        GLambdaIJ = np.zeros( (self.n,)*4 )
        GmuAI = np.zeros( (self.n,)*2 )
        GmuCI = np.zeros( (self.n,)*2 )
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)

    #Augmented Lagrangian
    def Augmented_Lagrangian_twochemical(self,X):
        """Augmented Lagrangian (a mix between Lagrangian and penalty function)"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.Hamiltonian_Energy(X) \
            + 0.5*self.rho * ( (self.A0_Constraints(A0) + muA0/self.rho )**2 + (self.C0_Constraints(C0) + muC0/self.rho )**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0) + Lambda0/self.rho, self.n2 )**2 ) \
            + np.sum( np.reshape( self.AIJ_Constraints(AIJ,A0,C0) + muAI/self.rho, self.n2 )**2 ) \
            + np.sum( np.reshape( self.CIJ_Constraints(CIJ,A0,C0) + muCI/self.rho, self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0) + LambdaIJ/self.rho, self.n4 )**2 )) \

    #Gradient of Augmented Lagrangian
    def Gradient_Augmented_Lagrangian_twochemical(self,X):
        """Gradient of the Augmented Lagrangian"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        GX = self.Gradient_Energy(X)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        GA0 += (self.rho * self.A0_Constraints(A0) + muA0) * self.Gradient_A0_Constraints(A0)
        GC0 += (self.rho * self.C0_Constraints(C0) + muC0) * self.Gradient_C0_Constraints(C0)
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.rho*self.D0_Constraints(A0,C0)+Lambda0)
        GA0 += gA0; GC0 += gC0
        gAIJ,gA0,gC0 = self.Gradient_AIJ_Constraints(AIJ,A0,C0,muAI=self.rho*self.AIJ_Constraints(AIJ,A0,C0)+muAI)
        GAIJ += gAIJ; GA0 += gA0; GC0 += gC0
        gCIJ,gA0,gC0 = self.Gradient_CIJ_Constraints(CIJ,A0,C0,muCI=self.rho*self.CIJ_Constraints(CIJ,A0,C0)+muCI)
        GCIJ += gCIJ; GA0 += gA0; GC0 += gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0,\
                                LambdaIJ=self.rho*self.DIJ_Constraints(AIJ,CIJ,A0,C0)+LambdaIJ)
        GAIJ += gAIJ; GCIJ += gCIJ; GA0 += gA0; GC0 += gC0
        GmuA0 = (self.A0_Constraints(A0) + muA0/self.rho)
        GmuC0 = (self.C0_Constraints(C0) + muC0/self.rho)
        GLambda0 = (self.D0_Constraints(A0,C0) + Lambda0/self.rho)
        GLambdaIJ = (self.DIJ_Constraints(AIJ,CIJ,A0,C0) + LambdaIJ/self.rho)
        GmuAI = (self.AIJ_Constraints(AIJ,A0,C0) + muAI/self.rho)
        GmuCI = (self.CIJ_Constraints(CIJ,A0,C0) + muCI/self.rho)
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)

    
    def Augmented_Lagrangian_onechemical(self,X):
        """Augmented Lagrangian (a mix between Lagrangian and penalty function)"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        return self.Hamiltonian_Energy(X) \
            + 0.5*self.rho * (  (self.AC0_Constraints(A0,C0) + muA0/self.rho )**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0) + Lambda0/self.rho, self.n2 )**2 ) \
            + np.sum( np.reshape( self.ACIJ_Constraints(AIJ,CIJ,A0,C0) + muAI/self.rho, self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0) + LambdaIJ/self.rho, self.n4 )**2 ))

    #Gradient of Augmented Lagrangian
    def Gradient_Augmented_Lagrangian_onechemical(self,X):
        """Gradient of the Augmented Lagrangian"""
        A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = self.Depack_X(X)
        GX = self.Gradient_Energy(X)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        gA0, gC0 = self.Gradient_AC0_Constraints(A0,C0,muA0=(self.rho*self.AC0_Constraints(A0,C0) + muA0))
        GA0 += gA0; GC0 += gC0
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.rho*self.D0_Constraints(A0,C0) + Lambda0)
        GA0 += gA0; GC0 += gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_ACIJ_Constraints(AIJ,CIJ,A0,C0, muAI=self.rho*self.ACIJ_Constraints(AIJ,CIJ,A0,C0) + muAI)
        GAIJ += gAIJ; GCIJ += gCIJ; GA0 += gA0; GC0 += gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0, LambdaIJ=self.rho*self.DIJ_Constraints(AIJ,CIJ,A0,C0) + LambdaIJ)
        GAIJ += gAIJ; GCIJ += gCIJ; GA0 += gA0; GC0 += gC0
        GmuA0 = (self.AC0_Constraints(A0,C0) + muA0/self.rho)
        GmuC0 = 0.0
        GLambda0 = (self.D0_Constraints(A0,C0) + Lambda0/self.rho)
        GLambdaIJ = (self.DIJ_Constraints(AIJ,CIJ,A0,C0) + LambdaIJ/self.rho)
        GmuAI = (self.ACIJ_Constraints(AIJ,CIJ,A0,C0) + muAI/self.rho)
        GmuCI = np.zeros( (self.n,self.n) )
        return self.Pack_X(GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI)


    #Energy only for ACDM parameters
    def Hamiltonian_Energy_XP(self,XP):
        """Total energy"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        return self.TH1S + np.einsum('iI,im,Im->',self.H1S,A0,A0) - np.einsum('iI,im,Im->',self.H1S,C0,C0) \
                + 0.5*( np.einsum('iIjJ,ijm,IJm->',self.H2S,AIJ,AIJ) - np.einsum('iIjJ,iJm,Ijm->',self.H2S,CIJ,CIJ) )

    #Gradient of Energy only for ACDM parameters
    def Gradient_Energy_XP(self,XP):
        """Gradient of the total energy without constraints"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        HH = self.H1S
        GA0 =   np.einsum('ij,jm->im',HH,A0) + np.einsum('ij,im->jm',HH,A0)
        GC0 = - np.einsum('ij,jm->im',HH,C0) - np.einsum('ij,im->jm',HH,C0)
        HH = 0.5*self.H2S
        GAIJ =   np.einsum('iIjJ,IJm->ijm',HH,AIJ) + np.einsum('iIjJ,ijm->IJm',HH,AIJ)
        GCIJ = - np.einsum('iIjJ,Ijm->iJm',HH,CIJ) - np.einsum('iIjJ,iJm->Ijm',HH,CIJ)
        #AIJ[i,j] = - AIJ[j,i] so the gradient...
        GAIJ += - np.einsum('ijm->jim',GAIJ)
        return self.Pack_XP(GA0,GC0,GAIJ,GCIJ)

    #Augmented Lagrangian only for ACDM parameters
    def Augmented_Lagrangian_XP(self,XP,XL):
        """Augmented Lagrangian only for ACDM parameters"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = self.Depack_XL(XL)
        XX = self.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        return self.Augmented_Lagrangian(XX)

    #Gradient of Augmented Lagrangian only for ACDM parameters
    def Gradient_Augmented_Lagrangian_XP(self,XP,XL):
        """Gradient of the Augmented Lagrangian only for ACDM parameters"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        Lambda0,muA0,muC0,LambdaIJ,muAI,muCI = self.Depack_XL(XL)
        XX = self.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
        GX = self.Gradient_Augmented_Lagrangian(XX)
        GA0,GC0,GLambda0,GmuA0,GmuC0,GAIJ,GCIJ,GLambdaIJ,GmuAI,GmuCI = self.Depack_X(GX)
        return self.Pack_XP(GA0,GC0,GAIJ,GCIJ)

    #Penalty Method only for ACDM parameters
    def Penalty_Method_XP(self,XP):
        """Penalty function adding to the total energy the square of the constraints"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        return self.Hamiltonian_Energy_XP(XP) \
            + 0.5*self.rho * ( self.A0_Constraints(A0)**2 + self.C0_Constraints(C0)**2 \
            + np.sum( np.reshape( self.D0_Constraints(A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.AIJ_Constraints(AIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.CIJ_Constraints(CIJ,A0,C0), self.n2 )**2 ) \
            + np.sum( np.reshape( self.DIJ_Constraints(AIJ,CIJ,A0,C0), self.n2*self.n2 )**2 ) )

    def Gradient_Penalty_Method_XP(self,XP):
        """Gradient of the penalty function"""
        A0,C0,AIJ,CIJ = self.Depack_XP(XP)
        GXP = self.Gradient_Energy_XP(XP)
        GA0,GC0,GAIJ,GCIJ = self.Depack_XP(GXP)
        GA0 += self.rho * self.A0_Constraints(A0) * self.Gradient_A0_Constraints(A0)
        GC0 += self.rho * self.C0_Constraints(C0) * self.Gradient_C0_Constraints(C0)
        gA0,gC0 = self.Gradient_D0_Constraints(A0,C0,Lambda0=self.D0_Constraints(A0,C0))
        GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gA0,gC0 = self.Gradient_AIJ_Constraints(AIJ,A0,C0,muAI=self.AIJ_Constraints(AIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gCIJ,gA0,gC0 = self.Gradient_CIJ_Constraints(CIJ,A0,C0,muCI=self.CIJ_Constraints(CIJ,A0,C0))
        GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        gAIJ,gCIJ,gA0,gC0 = self.Gradient_DIJ_Constraints(AIJ,CIJ,A0,C0,LambdaIJ=self.DIJ_Constraints(AIJ,CIJ,A0,C0))
        GAIJ += self.rho*gAIJ; GCIJ += self.rho*gCIJ; GA0 += self.rho*gA0; GC0 += self.rho*gC0
        return self.Pack_XP(GA0,GC0,GAIJ,GCIJ)


    #Information about acdm
    def info_acdm(self):
        ACDM.info_acdm(self)

#class CMP(ACDM):
#    """Class for bosons"""
#    def __init__(self,ns,Ns,h1,h2,rectangle=False,onechemical=True):
#        ACDM.__init__(self,ns,Ns,h1,h2,method="nospin",rectangle=rectangle,onechemical=onechemical)
#        self.fac = -1.0

#General routines


#Do QR decomposition to have lower triangular matrices
def QR_decomposition(acdm,A0,C0,AIJ,CIJ):
    """Do a QR decomposition to have lower triangular matrices"""
    #--------- QR decomposition to have a lower triangular matrix
    Q,A0 = np.linalg.qr(A0.T); A0 = A0.T
    AA0 = np.einsum('im,Im->iI',A0,A0)
    #dC0 is n or 1
    if acdm.dC0 == 1:
        #This is sometimes not possible!!
        #Use a SVD and select the non-zero eigenvalue
        CC0 = np.einsum('im,Im->iI',C0,C0)
        L,U = np.linalg.eigh(CC0)
        C0 = np.einsum('l,jl->jl',np.sqrt(L[-1:]),U[:,-1:])
        a = np.linalg.norm( np.einsum('im,Im->iI',C0,C0) - CC0)
        if abs(a) >1.e-9:
            print('QR decomposition dC0==1, we loose hole density',a)
    else:
        #Perform QR decomposition
        Q,C0 = np.linalg.qr(C0.T); C0 = C0.T
    #--Second do the QR decomposition of AIJ
    aIJ = np.zeros( (acdm.nAIJ,acdm.dAIJ) )
    for aij,(i,j) in enumerate(zip(*acdm.indAIJ)):
        aIJ[aij,:] = AIJ[i,j,:]
    Q,aIJ = np.linalg.qr(aIJ.T); aIJ = aIJ.T
    AIJ = np.zeros( (acdm.n,acdm.n,acdm.dAIJ) )
    for aij,(i,j) in enumerate(zip(*acdm.indAIJ)):
            AIJ[i,j,:acdm.dAIJ] =  aIJ[aij,:acdm.dAIJ]
            AIJ[j,i,:acdm.dAIJ] = -aIJ[aij,:acdm.dAIJ]
    #-- Third QR decomposition of CIJ
    cIJ = np.reshape(CIJ,(acdm.nCIJ,acdm.dCIJ))
    Q,cIJ = np.linalg.qr(cIJ.T); cIJ = cIJ.T
    CIJ = np.reshape(cIJ,(acdm.n,acdm.n,acdm.dCIJ))
    return (A0,C0,AIJ,CIJ)


#Build a new ACDM from 2 other ACDMs via convexity of the 2 corresponding RDM
def acdm_convex(acdm,a1,a2,A1,C1,AIJ1,CIJ1,A2,C2,AIJ2,CIJ2):
    """Build a new ACDMs coming from the 2-RDM linear combination of 2 ACDMs"""
    #---- Check ACDM conditions for both sets
    header = 'acdm_convex -- set 1'
    print(header,'A0_Constraints',acdm.A0_Constraints(A1))
    print(header,'C0_Constraints',acdm.C0_Constraints(C1))
    print(header,'D0_Constraints',np.linalg.norm(acdm.D0_Constraints(A1,C1)))
    print(header,'AIJ_Constraints',np.linalg.norm(acdm.AIJ_Constraints(AIJ1,A1,C1)))
    print(header,'CIJ_Constraints',np.linalg.norm(acdm.CIJ_Constraints(CIJ1,A1,C1)))
    print(header,'DIJ_Constraints',np.linalg.norm(acdm.DIJ_Constraints(AIJ1,CIJ1,A1,C1)))
    header = 'acdm_convex -- set 2'
    print(header,'A0_Constraints',acdm.A0_Constraints(A2))
    print(header,'C0_Constraints',acdm.C0_Constraints(C2))
    print(header,'D0_Constraints',np.linalg.norm(acdm.D0_Constraints(A2,C2)))
    print(header,'AIJ_Constraints',np.linalg.norm(acdm.AIJ_Constraints(AIJ2,A2,C2)))
    print(header,'CIJ_Constraints',np.linalg.norm(acdm.CIJ_Constraints(CIJ2,A2,C2)))
    print(header,'DIJ_Constraints',np.linalg.norm(acdm.DIJ_Constraints(AIJ2,CIJ2,A2,C2)))
    #Build new convex set
    AA0 = a1*np.einsum('im,Im->iI',A1,A1) + a2*np.einsum('im,Im->iI',A2,A2)
    print('acdm_convex A0_Constraints',np.linalg.norm( np.einsum('ii->',AA0) - acdm.N))
    #Perform Cholesky to have a lower triangular matrix
    try:
        A0 = np.linalg.cholesky( AA0 + 1.e-16*np.eye(acdm.nA0) )
    except np.linalg.LinAlgError:
        #Calculate the eigenvalues
        L = np.linalg.eigvals(AA0)
        raise np.linalg.LinAlgError('Eigenvalues of aaIJ' + str(L) \
                                    + '\nThe matrix is not definite positive')
    CC0 = a1*np.einsum('im,Im->iI',C1,C1) + a2*np.einsum('im,Im->iI',C2,C2)
    D0 = AA0 + CC0 - np.eye(acdm.n)
    print('acdm_convex D0_Constraints',np.linalg.norm(D0))
    try:
        C0 = np.linalg.cholesky( CC0 + 1.e-16*np.eye(acdm.nC0) )
    except np.linalg.LinAlgError:
        #Calculate the eigenvalues
        L = np.linalg.eigvals(CC0)
        raise np.linalg.LinAlgError('Eigenvalues of aaIJ' + str(L) \
                                    + '\nThe matrix is not definite positive')
    #Build the nAIJ,nAIJ overlap matrix to do a Cholesky decomposition
    AAIJ0 = a1*np.einsum('ijm,IJm->iIjJ',AIJ1,AIJ1) + a2*np.einsum('ijm,IJm->iIjJ',AIJ2,AIJ2)
    DD = np.einsum('iIjj->iI',AAIJ0) - AA0*np.float64(acdm.N-1)
    print('acdm_convex AIJ_Constraints',np.linalg.norm(DD))
    aaIJ = np.zeros( (acdm.nAIJ,acdm.nAIJ) )
    for aij,(i,j) in enumerate(zip(*acdm.indAIJ)):
        for Aij,(I,J) in enumerate(zip(*acdm.indAIJ)):
            aaIJ[aij,Aij] = AAIJ0[i,I,j,J]
    try:
        aIJ = np.linalg.cholesky( aaIJ + 1.e-16*np.eye(acdm.nAIJ) )
    except np.linalg.LinAlgError:
        #Calculate the eigenvalues
        L = np.linalg.eigvals(aaIJ)
        raise np.linalg.LinAlgError('Eigenvalues of aaIJ ' + str(L) \
                                    + '\nThe matrix is not definite positive')
    AIJ = np.zeros( (acdm.n,acdm.n,acdm.dAIJ) )
    for aij,(i,j) in enumerate(zip(*acdm.indAIJ)):
            AIJ[i,j,:acdm.dAIJ] =  aIJ[aij,:acdm.dAIJ]
            AIJ[j,i,:acdm.dAIJ] = -aIJ[aij,:acdm.dAIJ]
    #-- Finally same things with CIJ
    CCIJ0 = a1*np.einsum('ijm,IJm->iIjJ',CIJ1,CIJ1) + a2*np.einsum('ijm,IJm->iIjJ',CIJ2,CIJ2)
    DD = np.einsum('iIjj->iI',CCIJ0) - AA0*np.float64(acdm.n-(acdm.N-1))
    print('acdm_convex CIJ_Constraints',np.linalg.norm(DD))
    DIJ = AAIJ0 + np.einsum('iIjJ->iIJj',CCIJ0) - np.einsum('iI,jJ->iIjJ',AA0,np.eye(acdm.n))
    print('acdm_convex DIJ_Constraints',np.linalg.norm(DIJ))
    ccIJ = np.reshape(np.einsum('iIjJ->ijIJ',CCIJ0),(acdm.nCIJ,acdm.nCIJ))
    #To keep symmetry (can see as an overlap matrix)
    ccIJ = 0.5*( ccIJ + np.einsum('ij->ji',ccIJ) )
    #try:
    #    cIJ = np.linalg.cholesky( ccIJ + 1.e-12*np.eye(acdm.nCIJ) )
    #except np.linalg.LinAlgError:
    #    #Calculate the eigenvalues
    #    L = np.linalg.eigvals(ccIJ)
    #    raise np.linalg.LinAlgError('Eigenvalues of ccIJ' + str(L) \
    #                                + '\nThe matrix is not definite positive')
    #Better to use a SVD + QR decomposition
    L,U = np.linalg.eigh(ccIJ)
    #print('iljl U L U.T',np.linalg.norm( np.einsum('il,l,jl->ij',U,L,U) - ccIJ ) )
    #https://math.stackexchange.com/questions/3247639/rectangular-cholesky-decomposition-of-lower-dimension
    #  The Eckart-Young-Mirsky theorem tells us that the best rank r approximation to a possibly non-symmetric matrix A in the 2-norm 
    #  (also in the Frobenius norm) is given by the first k singular values and singular vectors of the SVD of A=UΣVT as A=∑ki=1σiUiVTi.
    #  When A is symmetric and positive semidefinite, this simplifies to A=∑ki=1σiUiUTi and this can be written as A=XXT
    #So we select only the first dCIJ singular values
    #Normally we have only self.dCIJ non-zero eigenvalues
    for i in range(acdm.nCIJ):
        if abs(L[i]) < 1.e-12:
            L[i] = 0.0
            #print(L)
    #Sort 
    ind = list(np.argsort(L))
    indCIJ = ind[-acdm.dCIJ:]
    #print(ind)
    L = np.sqrt(L)
    cIJ = np.einsum('l,jl->jl',L[indCIJ],U[:,indCIJ])
    CIJ = np.reshape(cIJ,(acdm.n,acdm.n,acdm.dCIJ))
    #print(cIJ.shape, CIJ.shape)
    CCIJ0 = np.einsum('ijm,IJm->iIjJ',CIJ,CIJ)
    DIJ = AAIJ0 + np.einsum('iIjJ->iIJj',CCIJ0) - np.einsum('iI,jJ->iIjJ',AA0,np.eye(acdm.n))
    print('acdm_convex DIJ_Constraints',np.linalg.norm(DIJ))
    #Finally  do a QR decomposition to have triangular matrices
    print('CC0-C0.C0',np.linalg.norm( np.einsum('im,Im->iI',C0,C0) - CC0))
    A0,C0,AIJ,CIJ = QR_decomposition(acdm,A0,C0,AIJ,CIJ)
    print('CC0-C0.C0 QR',np.linalg.norm( np.einsum('im,Im->iI',C0,C0) - CC0))
    return A0,C0,AIJ,CIJ


#Testing the gradient
def test_Gradient(func,dfunc,label,X0,d,only_diff=False):
    """Plot the difference between the analytical gradient and the numerical one to test"""
    text,pos = label()
    L0 = func(X0)
    GX0 = dfunc(X0)
    Gnum = np.zeros(len(X0))
    #To label the abscissae
    #
    p = 0
    for l in range(len(X0)):
        X1 = X0.copy()
        X1[l] += d
        L1 = func(X1)
        Gnum[l] = (L1-L0)/d
        #print(l,X0[l],X1[l],L0,L1,GX0[l],Gnum[l])
        dd = abs(GX0[l] - Gnum[l])
        if p < len(pos)-1 and l>=pos[p+1]:
            p += 1
    import matplotlib.pyplot as plt
    from matplotlib.pyplot import figure
    figure(figsize=(16, 6), dpi=80)
    x = np.array(range(len(X0)))
    y = GX0 - Gnum
    for p,t in zip(pos,text):
        plt.text(p,0,t,rotation=90)
    if not only_diff:
        plt.plot(x, GX0,label='gradient')
        plt.plot(x, Gnum,'--',label='numeric')
    plt.plot(x,y,label='difference')
    plt.legend()
    plt.show()
    print('Difference',np.linalg.norm(y))


#Testing Pack_X and Depack_X
def test_Pack_X(acdm,random=True):
    """Test the packing and depacking of arrays (Pack_X and Depack_X)"""
    if random:
        X0 = np.random.random( acdm.Size_X() )
    else:
        X0 = acdm.Initial_X()
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    X1 = acdm.Pack_X(A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI)
    print(np.linalg.norm(X0-X1))
    for (a,b) in zip(acdm.Depack_X(X0),acdm.Depack_X(X1)):
        print(np.linalg.norm(a-b))


#Test all gradients
def testall_gradients(acdm,XX=False,d=0.00001,header=''):
    """Test the gradients"""
    print('Test of all gradients')
    if XX == False:
        XX = np.random.random(acdm.Size_X())
    print(header+'Energy');     test_Gradient(acdm.Hamiltonian_Energy,acdm.Gradient_Energy,acdm.label,XX,d)
    print(header+'Lagrangian'); test_Gradient(acdm.Lagrangian,acdm.Gradient_Lagrangian,acdm.label,XX,d)
    print(header+'EN+Lag');     test_Gradient(acdm.Penalty_Energy_Lagrangian,acdm.Gradient_Penalty_Energy_Lagrangian,acdm.label,XX,d)
    print(header+'Penalty');    test_Gradient(acdm.Penalty_Method,acdm.Gradient_Penalty_Method,acdm.label,XX,d)
    print(header+'Augmented');  test_Gradient(acdm.Augmented_Lagrangian,acdm.Gradient_Augmented_Lagrangian,acdm.label,XX,d)
    print(header+'Square');     test_Gradient(acdm.Penalty_Square,acdm.Gradient_Penalty_Square,acdm.label,XX,d)


#Information routines
def info_acdm(acdm,header=''):
    """Information about the structure of acdm"""
    print(header+'method',acdm.method,'n',acdm.n,'N',acdm.N,'ns',acdm.ns,'Ns',acdm.Ns,'acdm.rho',acdm.rho)
    print(header+'rectangle',acdm.rectangle,'onechemical',acdm.onechemical)
    print(header+'Number of vectors  A0',acdm.nA0,  ', size of the vectors', acdm.dA0, ', number of components for A0:',acdm.pA0)
    print(header+'Number of vectors  C0',acdm.nC0,  ', size of the vectors', acdm.dC0, ', number of components for C0:',acdm.pC0)
    print(header+'Number of vectors AIJ',acdm.nAIJ, ', size of the vectors', acdm.dAIJ, ', number of components',acdm.pAIJ)
    print(header+'Number of vectors CIJ',acdm.nCIJ, ', size of the vectors', acdm.dCIJ, ', number of components',acdm.pCIJ)
    print(header+'Total number of parameters for the Lagrangian (A0,C0,lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI):',acdm.Size_X())


def info_constraints(acdm,X0,header='',output=False,verbose=True):
    """Information about constraints, if output return the constraints"""
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    diff = dict()
    diff['A0'] = acdm.A0_Constraints(A0)
    diff['C0'] = acdm.C0_Constraints(C0)
    diff['AC0'] = acdm.AC0_Constraints(A0,C0)
    diff['AIJ'] = max(abs(np.reshape(acdm.AIJ_Constraints(AIJ,A0,C0),acdm.n2)))
    diff['CIJ'] = max(abs(np.reshape(acdm.CIJ_Constraints(CIJ,A0,C0),acdm.n2)))
    diff['ACIJ'] = max(abs(np.reshape(acdm.ACIJ_Constraints(AIJ,CIJ,A0,C0),acdm.n2)))
    diff['D0'] = max(abs(np.reshape(acdm.D0_Constraints(A0,C0),acdm.n2)))
    diff['DIJ'] = max(abs(np.reshape(acdm.DIJ_Constraints(AIJ,CIJ,A0,C0),acdm.n4)))
    form = ' %9.2e'
    if verbose:
        print(header+'  A0_Constraints'+form % diff['A0'],  ' C0_Constraints'+form % diff['C0'],  ' AC0_Constraints'+form % diff['AC0'])
        print(header+' AIJ_Constraints'+form % diff['AIJ'], 'CIJ_Constraints'+form % diff['CIJ'], 'ACIJ_Constraints'+form % diff['ACIJ'] )
        print(header+'  D0_Constraints'+form % diff['D0'],  'DIJ_Constraints'+form % diff['DIJ'] )
    if output:
        return diff


def info_particles(acdm,X0,header=''):
    """Give information about particles for W0 and ai W0"""
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    AA0 = np.einsum('im,Im->iI',A0,A0)
    form = ' %7.3f'
    aa = np.trace(AA0)
    cc = np.trace(C0.T@C0)
    print(header+'Number of electrons [0]'+form % aa,'Number of holes [0]'+form % cc,'sum'+form % (aa+cc),'[%0d/%0d]' % (acdm.n,acdm.N) )
    for i in range(acdm.n):
        fai = np.trace(AIJ[i].T@AIJ[i])
        fci = np.trace(CIJ[i].T@CIJ[i])
        fi = AA0[i,i]
        if abs(fi) > 1.e-6:
            fai = fai / fi
            fci = fci / fi
            print( (header+'Number of electrons [%0d]'+form) % (i+1,fai), \
                  ('Number of holes [%0d]'+form) % (i+1,fci),'sum'+form % (fai+fci),'-- normalized (%4f)' % fi )
        else:
            print( (header+'Number of electrons [%0d]'+form) % (i+1,fai),('Number of holes [%0d]'+form) % (i+1,fci), \
                  'sum'+form %(fai+fci),'-- normalized (%4f)' % fi)


def info_energies(acdm,X0,header='', FCI_energy = 0.0,output=False,verbose=True,col=('white',)*6):
    """Information about the energy functionals"""
    ACDM_energy = acdm.Hamiltonian_Energy(X0)
    if verbose:
        print(header+'acdm.rho',acdm.rho)
    if FCI_energy != 0.0:
        energy_error = ACDM_energy - FCI_energy
        if verbose:
            print(header+'ACDM Total Energy         --',ACDM_energy, 'FCI energy',FCI_energy, 'difference', energy_error )
    else:
        energy_error = np.NaN
    diff = { 'energy': energy_error }
    diff['G_E'] = max(abs(acdm.Gradient_Energy(X0)))
    diff['G_L'] = max(abs(acdm.Gradient_Lagrangian(X0)))
    diff['G_P_E_L'] = max(abs(acdm.Gradient_Penalty_Energy_Lagrangian(X0)))
    diff['G_A_L'] = max(abs(acdm.Gradient_Augmented_Lagrangian(X0)))
    diff['G_P_L'] = max(abs(acdm.Gradient_Penalty_Method(X0)))
    diff['G_P_S'] = max(abs(acdm.Gradient_Penalty_Square(X0)))
    if verbose:
        print(header+tc.colored('Hamiltonian Energy        -- %21.16f -- gradient %12.6e' % (ACDM_energy,         diff['G_E']                    ),col[0]))
        print(header+tc.colored('Lagrangian                -- %21.16f -- gradient %12.6e' % (acdm.Lagrangian(X0), diff['G_L']                    ),col[1]))
        print(header+tc.colored('Penalty Energy Lagrangian -- %21.16f -- gradient %12.6e' % (acdm.Penalty_Energy_Lagrangian(X0), diff['G_P_E_L'] ),col[2]))
        print(header+tc.colored('Augmented Lagrangian      -- %21.16f -- gradient %12.6e' % (acdm.Augmented_Lagrangian(X0), diff['G_A_L']        ),col[3]))
        print(header+tc.colored('Penalty Method            -- %21.16f -- gradient %12.6e' % (acdm.Penalty_Method(X0), diff['G_P_L']          ),col[4]))
        print(header+tc.colored('Penalty Square Energy     -- %21.16f -- gradient %12.6e' % (acdm.Penalty_Square(X0), diff['G_P_S']              ),col[5]))
    if output:
        return diff


def info_all(acdm,X0,header='', FCI_energy=0.0,output=False,verbose=True,col=('white',)*6):
    """All information except about the acdm structure"""
    #info_acdm(acdm,header)
    if verbose:
        info_particles(acdm,X0,header)
    diff = info_constraints(acdm,X0,header,output=True,verbose=verbose)
    diff = diff | info_energies(acdm,X0,header, FCI_energy,output=True,verbose=verbose,col=col)
    if output:
        return diff
    

def info_gradients(acdm,X0,func=None):
    """Gradient information"""
    import matplotlib.pyplot as plt
    if func == None:
        func = acdm.Gradient_Lagrangian
    plt.figure(figsize=(16, 6), dpi=80)
    x = np.arange(acdm.Size_X())
    text,pos = acdm.label()
    for p,t in zip(pos,text):
        plt.text(p,0,t,rotation=90)
    plt.plot(x, func(X0))
    plt.show()

    
def info_gradient_lagrangian(acdm,X0,header='',output=False):
    """Some information about Lagrangian gradient"""
    diff = dict()
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    print(header+'-- Gradients related to Lagrangian')
    print(header+'Lagrangian',acdm.Lagrangian(X0),'gradient',max(abs(acdm.Gradient_Lagrangian(X0))))
    AA0 = np.einsum('im,Im->iI',A0,A0)
    CC0 = np.einsum('im,Im->iI',C0,C0)
    if acdm.onechemical:
        HH = acdm.H1S + 0.5*( np.float64( 2*(acdm.N-1) - acdm.n ) * muAI + np.einsum('iIjj->iI',LambdaIJ) )
        diff['H1.AA0'] = np.einsum('ij,ij->',   HH - muA0*np.eye(acdm.n) - Lambda0, AA0)
        diff['H1.CC0'] = np.einsum('ij,ij->', - HH + muA0*np.eye(acdm.n) - Lambda0, CC0)
        print(tc.colored(header+'(+H1S-muA0-Lambda0).AA0           %20.6e' % diff['H1.AA0'],'blue'))
        print(tc.colored(header+'(-H1S+muA0-Lambda0).CC0           %20.6e' % diff['H1.CC0'],'blue'))
        AAIJ = np.einsum('ijm,IJm->iIjJ',AIJ,AIJ)
        CCIJ = np.einsum('ijm,IJm->iIjJ',CIJ,CIJ)
        HH =   0.5*acdm.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muAI,np.eye(acdm.n))
        diff['H2.AAIJ'] = np.einsum('iIjJ,iIjJ->',HH,AAIJ)
        print(tc.colored(header+'(+0.5*H2S-LambdaIJ - muAI.I).AAIJ %20.6e' % diff['H2.AAIJ'],'blue'))
        HH = - 0.5*acdm.H2S - LambdaIJ + np.einsum('iI,jJ->iIjJ',muAI,np.eye(acdm.n))
        diff['H2.CCIJ'] = np.einsum('iIjJ,iIJj->',HH,CCIJ)
        print(tc.colored(header+'(-0.5*H2S-LambdaIJ + muAI.I).CCIJ %20.6e' % diff['H2.CCIJ'],'blue'))
    else:
        HH = acdm.H1S + 0.5*( np.float64(acdm.N-1) * muAI + np.float64(acdm.n-(acdm.N-1))*muCI + np.einsum('iIjj->iI',LambdaIJ) )
        diff['H1.AA0'] = np.einsum('ij,ij->',   HH - Lambda0 - muA0*np.eye(acdm.n), AA0)
        diff['H1.CC0'] = np.einsum('ij,ij->', - HH - Lambda0 - muC0*np.eye(acdm.n), CC0)
        print(tc.colored(header+'(+H1S-Lambda0-muA0).AA0         %20.6e' % diff['H1.AA0'],'blue'))
        print(tc.colored(header+'(-H1S-Lambda0-muC0).CC0         %20.6e' % diff['H1.CC0'],'blue'))
        AAIJ = np.einsum('ijm,IJm->iIjJ',AIJ,AIJ)
        CCIJ = np.einsum('ijm,IJm->iIjJ',CIJ,CIJ)
        HH =   0.5*acdm.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muAI,np.eye(acdm.n))
        diff['H2.AAIJ'] = np.einsum('iIjJ,iIjJ->',HH,AAIJ)
        print(tc.colored(header+'(+0.5*H2S-LambdaIJ-muAI.I).AAIJ %20.6e' % diff['H2.AAIJ'],'blue'))
        HH = - 0.5*acdm.H2S - LambdaIJ - np.einsum('iI,jJ->iIjJ',muCI,np.eye(acdm.n))
        diff['H2.CCIJ'] = np.einsum('iIjJ,iIJj->',HH,CCIJ)
        print(tc.colored(header+'(-0.5*H2S-LambdaIJ-muCI.I).CCIJ %20.6e' % diff['H2.CCIJ'],'blue'))
    if output:
        return diff


if __name__ == '__main__':
    import scipy.optimize
    np.random.seed(1) #For reproducibility
    ns = 7
    Ns = 3
    #Define Hamiltonian
    h1 = 2.0*np.random.random((ns,ns))
    h2 = -0.1*np.random.random((ns,)*4)
    # Restore permutation symmetry
    h1 = 0.5*(h1 + h1.T)
    h2 = 0.5*(h2 + h2.transpose(1,0,2,3))
    h2 = 0.5*(h2 + h2.transpose(0,1,3,2))
    h2 = 0.5*(h2 + h2.transpose(2,3,0,1))#To be changed as you like
    
    acdm = ACDM(ns=ns,Ns=Ns,h1=h1,h2=h2,method='nospin')

    #FCI solution
    from pyscf import fci
    H_fci = fci.direct_spin1.pspace(h1, h2, acdm.ns, acdm.nelec, np=acdm.ndet)[1]
    e_all, v_all = np.linalg.eigh(H_fci)
    e, fcivec = fci.direct_spin1.kernel(h1, h2, acdm.ns, acdm.nelec, nroots=2,
                                    max_space=30, max_cycle=100)
    print('FCI energy', e[0])
    print('wfn overlap', v_all[:,0].dot(fcivec[0].ravel()))
    rdm1, rdm2 = fci.direct_spin1.make_rdm12(fcivec[0], acdm.ns, acdm.nelec)
    (dm1a,dm1b),(dm2aa,dm2ab,dm2bb) = fci.direct_spin1.make_rdm12s(fcivec[0], acdm.ns, acdm.nelec)
    FCI_energy = np.einsum('ij,ij->',h1,rdm1)+0.5*np.einsum('ijkl,ijkl->',h2,rdm2)
    print('energy', e[0],FCI_energy)

    info_acdm(acdm)
    print('-'*30,'Start of Initialization')
    X0 = acdm.Initial_X()
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    print('Initial Energy target',acdm.Hamiltonian_Energy(X0), 'Lagrangian Initial target', acdm.Lagrangian(X0))
    print('Initial Lagrangian gradient',max(abs(acdm.Gradient_Lagrangian(X0))) )
    info_constraints(acdm,X0)

    sol = scipy.optimize.root(fun=acdm.Gradient_Lagrangian, x0=X0,method='krylov',\
                options={'disp': True, 'xatol': 1.e-9,'xtol': 1.e-4,'fatol': 1.e-6,'maxiter': 400, 'line_search': 'wolfe', \
                'jac_options': {'rdiff': 1.e-10, 'method': 'lgmres', 'inner_tol': 1.e-8}})
    
    X0 = sol.x
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)
    
    ier = sol.status
    mesg = sol.message
    print('Number of electrons',np.trace(A0.T@A0))
    print('Number of holes',np.trace(C0.T@C0))
    print('Final energy',acdm.Hamiltonian_Energy(X0), 'FCI energy',FCI_energy)
    print('Final Lagrangian gradient',max(abs(acdm.Gradient_Lagrangian(X0))) )
    print('Message of fsolve ier=%d:' % ier,mesg)
    print('Number of iterations',sol.nit)
    print('Max Gradient values',max(sol.fun))

    X0 = sol.x
    A0,C0,Lambda0,muA0,muC0,AIJ,CIJ,LambdaIJ,muAI,muCI = acdm.Depack_X(X0)

    aa = np.einsum('im,km->ik',A0[:ns,:],A0[:ns,:])
    cc = np.einsum('im,km->ik',C0,C0)
    aaIJ = np.einsum('ijm,klm->ikjl',AIJ,AIJ)

    assert(np.allclose(dm2aa+dm2ab+dm2ab.transpose(2,3,0,1)+dm2bb, rdm2))
    print('no spin beta',max(abs(np.reshape(dm1b,ns*ns))))

    N4_energy = acdm.Hamiltonian_Energy(X0)
    FCI_energy = np.einsum('ij,ij->',h1,rdm1)+0.5*np.einsum('ijkl,ijkl->',h2,rdm2)
    diff_energy = FCI_energy-N4_energy
    print('FCI,N4 energies  %21.17f %21.17f' % (FCI_energy,N4_energy),'diff',diff_energy,\
          tc.colored('percentage %7.5f%%' % (100.0*(1.0-abs(diff_energy/FCI_energy))),'red') )

    FCI_e1 = np.einsum('ij,ij->',h1,dm1a)
    N4_e1 = np.einsum('ij,ij->',h1,aa)
    diff_e1 = FCI_e1 - N4_e1
    print('FCI,N4 h1 energy %21.17f %21.17f' % (FCI_e1,N4_e1),'diff',FCI_e1-N4_e1,'percentage %7.5f%%' % (100.0*(1.0-abs(diff_e1/FCI_e1))) )

    FCI_e2 = 0.5*np.einsum('ijkl,ijkl->',h2,rdm2)
    N4_e2 = 0.5*np.einsum('ijkl,ijkl->',acdm.H2,aaIJ)
    diff_e2 = FCI_e2 - N4_e2
    print('FCI,N4 h2 energy %21.17f %21.17f' % (FCI_e2,N4_e2),'diff',FCI_e2-N4_e2,'percentage %7.5f%%' % (100.0*(1.0-abs(diff_e2/FCI_e2))) )

    print('diff RDM1',max(abs(np.reshape(aa-rdm1,ns*ns))))
    print('diff RDM2',max(abs(np.reshape(aaIJ-rdm2,ns*ns*ns*ns))))
