#!/usr/bin/env python
# coding: utf-8

# # Test of the ACDM method.
# 
# The ACDM Lagrangian associated to the two-body Hamiltonian is
# 
# $$
#     L \left[ D^{0}, \left\{ D^{i} \right\}_{i=1,n} \right]
#         = \sum_{ii'}^n h_{ii'} <{A^{0}_{i}},{A^{0}_{i'}}> + \sum_{ii';jj'}^n H^{ii'}_{jj'} <{A^{i}_{j}},{A^{i'}_{j'}}>
#         - {\underbrace{\mu_0 \left[ \sum_j^n <{A^{0}_{j}},{A^{0}_{j}}> - N \right]}_{\text{To have}\, N\,\text{electrons}}}
#         - {\underbrace{\sum_{ii'}^n \lambda_{ii'} \left[ <{A^{0}_{i}},{A^{0}_{i'}}> + <{C^{0}_{i'}},{C^{0}_{i}}> 
#                 - \delta_{ii'}\right]}_{\text{To have}\, {A^{0\top}} A^{0} = \,\mathbb{I}_n}}\\
#         - {\underbrace{\sum_{ii';jj'}^n \Lambda^{ii'}_{jj'} \left[  <{A^{i}_{j}},{A^{i'}_{j'}}> + <{C^{i}_{j'}},{C^{i'}_{j}}> 
#                 - \delta_{jj'}<{A^{0}_{i}},{A^{0}_{i'}}>\right]}_{\text{To have}\, {{A^{i}}^\top} A^{i'} + \left({C^{i\top}} C^{i'}\right)^\top = {}^1\Gamma_{ii'}\mathbb{I}_n}}
#         - \sum_{ii'}^n {\underbrace{\mu_{ii'} \left[ \sum_j <{A^{i}_{j}},{A^{i'}_{j}}> 
#                 - (N-1)<{A^{0}_{i}},{A^{0}_{i'}}>\right]}_{\text{To have}\, {}^1\Gamma_{ii'}(N-1)\,\text{electrons} }}
# $$
# 
# We unify nospin, spin and restricted spin considering by means of changing the packing and build the class ACDM.

# In[ ]:


# Two-body hamiltonian and AIJ[i,j] = - AIJ[j,i] and regular matrices
import numpy as np
from pyscf import fci
from scipy.special import binom  #Binom formula
import scipy.optimize

import termcolor as tc
import ACDM, ACDMOPT

from num2tex import num2tex

np.random.seed(1) #For reproducibility

imax = 1
fcdat = open('test-ACDM-Class-nospin.dat','w')
final_text = ""

for ns,Ns in [ (3,2), (5,2), (5,3), (7,4), (8,4), (9,2), (9,3), (9,5), (9,8), (11,3), (12,5), (12,9), (15,6), (15,10) ]:    
    for itest in range(imax):
        method = 'nospin'
        norb = ns; nelec = Ns
        header = "[itest=%d,norb=%d,nelec=%d] -- " % (itest,ns,Ns)
        print(header,'='*90)
        #Generate a random Hamiltonian
        h1,h2 = ACDMOPT.H_random(norb,Ns)
        acdm = ACDM.ACDM(ns=ns,Ns=Ns,h1=h1,h2=h2,method=method)
        #Calculate the FCI solution
        FCI = ACDMOPT.FCI(h1,h2,nelec,header=header)
        acdm = ACDM.ACDM(ns=norb,Ns=nelec,h1=h1,h2=h2,method="nospin",onechemical=True)
        XW = ACDMOPT.ACDM_from_pyscf(acdm,norb,nelec,FCI,multipliers=False,header=header)
        #Initial state is the FCI
        X0 = XW
        #X0 = acdm.Initial_X()
        ACDMOPT.FCI_compare_energies(acdm,X0,FCI,header=header+" <-- ")
        
        #Find a solution
        #sol = ACDMOPT.opt_lagrangian(acdm,X0,maxiter=300,header=header,disp=False)
        result = ACDMOPT.opt_penalty_method(acdm,X0,maxiter=300,header=header,disp=False,FCI_energy=FCI.energy,\
                                            rho=10000,rho_step=10000,tau=0.6,rho_max=10000000,tol=1.e-6)
        X0 = result['X0']
        
        #Check versus FCI solution
        diff = { 'norb': norb, 'nelec': nelec }
        diff = diff | ACDM.info_gradient_lagrangian(acdm,X0,header=header+' gradient ',output=True)
        diff = diff | ACDM.info_all(acdm,X0,header=header,output = True)
        diff = diff | ACDMOPT.FCI_compare_energies(acdm,X0,FCI,header=header+" >-- ",output=True)
        
        fcdat.write(str(diff))
        print(tc.colored(str(diff),'blue'))
        fcdat.flush()
        if itest == 0:
            Cdiff = diff
        else:
            for key,value in diff.items():
                Cdiff[key] += value
        #Latex output
        print('n N energy_error rdm1 rdm2 D0 A0 DIJ AIJ')
        text = '$%2d$ & $%2d$ & ' % (ns,NS)
        for n in  (diff['energy'],diff['rdm1'],diff['rdm2'],diff['D0'],diff['A0'],diff['DIJ'],diff['AIJ']):
            text += '$n = {}$'.format(numtext(n,precision=1))
        print(text+'\\')
    #Statistics
    for key,value in Cdiff.items():
        Cdiff[key] = value/imax
    text = header+str(Cdiff)
    print(tc.colored('+'*20+'> '+text,'green'))
    final_text = text+'\n'
print(tc.colored(final_text,'red'))
    

