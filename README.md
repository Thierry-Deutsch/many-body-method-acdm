# Many Body method: Pair of Anti-Commutant Density Matrices (ACDM)

New method with a complexity n⁶ to solve the many-body problem where n is the number of states and N the number of
electrons.
We give some scripts as calculation kernels for chemistry or solid state physics.

This repository contains some scripts which implement a new method to solve the many-body problem with n⁴ parameters and
with a n⁶ complexity based on a Lagrangian.

This method is based on finding the roots of this Lagrangian:
![Lagrangian](doc/lagrangian.png "Lagrangian of the method")
where $`A^0`$, $`C^0`$ are square matrices of $`n\times n`$ ($`A^0_i`$ indicates a row),
and $`A^i`$ $`n`$ matrices of size $`\left(\binom{n}{2}\right)\times n`$ with $`A^i_j = - A^j_i`$
and $`C^i`$ $`n`$ matrices of size $`n^2\times n`$.
$`\lambda`$ and $`\mu_0`$ are Lagrange multipliers associated to the constraints for $`A^0`$ and $`C^0`$,
and $`\Lambda`$, $`\mu`$ for the set of $`n`$ $`A^i`$ and $`C^i`$ matrices.

In the repository doc, we give a presentation which explains the method. An article is also available in
[arXiv](https://arxiv.org/abs/2111.15281) and [a video of a presentation](http://gdr-rest.polytechnique.fr/node/146). 

## Description of the files

* `ACDM-one-body-test.ipynb`, a jupyter notebook which solves the one-body hamiltonien with an ACDM $`D = (A,C)`$ two matrices
  of $`n\times n`$ with two constraints (no use of the class ACDM);
* `ACDM-nospin-pyscf-test.ipynb` jupyter notebook to solve two-body H2 without spin (no use of the class ACDM);
* `ACDM.py`          Define the class ACDM unifying nospin, spin, and restricted spin (spin and rspin are not working);
* `ACDMOPT.py`       Define the optimization and some useful operation;
* `Wavefunctions.py` Define the class to handle the wavefunctions and many density matrices, used to test the full CI solution;
* `Wavefunctions-pyscf.ipynb` Start for the full CI, determine the Lagrangian multipliers from the least square method;
    and check that the algorithm stays at the full CI solution (it is a way to check that the conditions are suffcient);
* `ACDM-numerical-results-article.py`: Script to perform the numerical results in the article (22/08/2022);
* `doc/2022-07-08-presentation-webinar-GdR-REST.pdf` presentation 08/07/2022.
