#!/usr/bin/env/python

# Statistics of all calculations
# Date: 07/03/2022

import numpy as np

fcdat = open('test-ACDM-Class-nospin.dat','r')
Cdiff_energy = 0.0
Cdiff_e1 = 0.0
Cdiff_e2 = 0.0
Cdiff_rdm1 = 0.0
Cdiff_rdm2 = 0.0
Cdiff_D0 = 0.0
Cdiff_A0 = 0.0
Cdiff_DIJ = 0.0
Cdiff_AIJ = 0.0
Cdiff = dict()

for i,line in enumerate(fcdat):
    #print(i,line[:-1])
    diff = np.float64(line.split())
    n = int(diff[0])
    N = int(diff[1])
    if not (n,N) in Cdiff:
        Cdiff[(n,N)] = np.zeros(10)
    Cdiff[(n,N)][0] += 1.0
    Cdiff[(n,N)][1:] += abs(diff[4:])
    #ns,Ns,FCI_energy,N4_energy,diff_energy,diff_e1,diff_e2,diff_rdm1,diff_rdm2,diff_D0,diff_A0,diff_DIJ,diff_AIJ = np.float64(line.split())
    #print(ns,Ns,FCI_energy,N4_energy,diff_energy,diff_e1,diff_e2,diff_rdm1,diff_rdm2,diff_D0,diff_A0,diff_DIJ,diff_AIJ)

# diff[2] = FCI_energy
# diff[3] = N4_energy
print('(n,N) #tests diff_E diff_e1 diff_e2 diff_rdm1 diff_rdm2 diff_D0 diff_A0 diff_DIJ diff_AIJ')
for k,v in Cdiff.items():
    print(k,'%d - ' % int(v[0]), np.array2string(v[1:]/v[0],max_line_width=200) )
